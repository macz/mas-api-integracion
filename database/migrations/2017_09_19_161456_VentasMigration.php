<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VentasMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('servidor_id');
            $table->bigInteger('pedido_id');
            $table->bigInteger('linea_id');
            $table->string('factura', 300);
            $table->date('fecha');
            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_fin')->nullable();
            $table->bigInteger('vendedor_id');
            $table->string('vendedor_codigo', 100);
            $table->string('vendedor_nombre', 300);
            $table->bigInteger('ruta_id')->nullable();
            $table->string('ruta_codigo', 100)->nullable();
            $table->string('ruta_nombre', 300)->nullable();
            $table->enum('estado', ['Finalizado', 'Anulado', 'En Proceso', 'En Ruta', 'Por Entregar']);
            $table->bigInteger('cliente_id');
            $table->string('cliente_codigo', 100)->nullable();
            $table->string('cliente_nombre', 300);
            $table->string('cliente_nit', 20);
            $table->string('cliente_direccion', 300);
            $table->integer('no_linea');
            $table->decimal('cantidad', 12,4)->unsigned();
            $table->bigInteger('producto_id');
            $table->string('producto_codigo', 100);
            $table->string('producto_nombre', 300);
            $table->string('detalle', 300);
            $table->bigInteger('unidad_id');
            $table->string('unidad_nombre', 100);
            $table->decimal('unidad_base', 12,4)->unsigned();
            $table->decimal('precio', 12,4)->unsigned();
            $table->decimal('costo', 12,4)->unsigned();
            $table->decimal('descuento', 12,4)->unsigned();
            $table->bigInteger('lista_id')->nullable();
            $table->string('lista_nombre', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index('servidor_id');
            $table->index('pedido_id');
            $table->index('linea_id');
            $table->index('cliente_id');
            $table->index('producto_id');
            $table->index('producto_codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
