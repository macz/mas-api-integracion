<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InventariosMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('servidor_id');
            $table->bigInteger('producto_id');
            $table->string('codigo', 100);
            $table->string('codigo_proveedor', 100)->nullable();
            $table->string('nombre', 300);
            $table->bigInteger('proveedor_id');
            $table->string('proveedor_codigo', 100);
            $table->string('proveedor_nombre', 300);
            $table->bigInteger('sucursal_id');
            $table->string('sucursal_codigo', 100);
            $table->string('sucursal_nombre', 300);
            $table->bigInteger('bodega_id');
            $table->string('bodega_nombre', 100);
            $table->bigInteger('unidad_id');
            $table->string('unidad_nombre', 100);
            $table->decimal('unidad_base', 12,4)->unsigned();
            $table->decimal('costo', 12,4)->unsigned();
            $table->boolean('activo')->default(true);
            $table->decimal('existencia', 12,4);
            $table->date('ultima_compra')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
            $table->index('servidor_id');
            $table->index('proveedor_id');
            $table->index('producto_id');
            $table->index('bodega_id');
            $table->index('unidad_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventarios');
    }
}
