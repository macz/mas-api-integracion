<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cliente_id');
            $table->bigInteger('servidor_id');
            $table->string('codigo', 100)->nullable();
            $table->string('nombre_comercial', 100);
            $table->string('razon_social', 100)->nullable();
            $table->string('nit', 20);
            $table->string('direccion', 300);
            $table->string('dpi', 100)->nullable();
            $table->string('telefono_1', 20)->nullable();
            $table->string('telefono_2', 20)->nullable();
            $table->string('email', 300)->nullable();
            $table->boolean('activo')->default(true);
            $table->string('geopos', 100)->nullable();
            $table->bigInteger('vendedor_id')->nullable();
            $table->string('vendedor_codigo', 100)->nullable();
            $table->string('vendedor_nombre', 300)->nullable();
            $table->bigInteger('ruta_id')->nullable();
            $table->string('ruta_codigo', 100)->nullable();
            $table->string('ruta_nombre', 300)->nullable();
            $table->bigInteger('canal_id')->nullable();
            $table->string('canal_codigo', 100)->nullable();
            $table->string('canal_nombre', 300)->nullable();
            $table->bigInteger('ciudad_id')->nullable();
            $table->string('ciudad_codigo', 100)->nullable();
            $table->string('ciudad_nombre', 300)->nullable();
            $table->bigInteger('region_id')->nullable();
            $table->string('region_codigo', 100)->nullable();
            $table->string('region_nombre', 300)->nullable();
            $table->bigInteger('municipio_id')->nullable();
            $table->string('municipio_codigo', 100)->nullable();
            $table->string('municipio_nombre', 300)->nullable();
            $table->bigInteger('departamento_id')->nullable();
            $table->string('departamento_codigo', 100)->nullable();
            $table->string('departamento_nombre', 300)->nullable();
            $table->bigInteger('lista_id')->nullable();
            $table->string('lista_nombre', 300)->nullable();
            $table->bigInteger('tipo_id')->nullable();
            $table->string('tipo_nombre', 300)->nullable();
            
            $table->softDeletes();
            $table->timestamps();
            $table->index('servidor_id');
            $table->index('cliente_id');
            $table->index('vendedor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
