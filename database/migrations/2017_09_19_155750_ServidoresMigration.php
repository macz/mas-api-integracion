<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServidoresMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('servidores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('servidor_id');
            $table->bigInteger('sucursal_id');
            $table->string('sucursal_codigo', 100);
            $table->string('sucursal_nombre', 100);
            $table->timestamp('ultima_sincronizacion');
            $table->softDeletes();
            $table->timestamps();
            $table->index('servidor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servidores');
    }
}
