<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            Commands\SincroData::class,
            Commands\SendData::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //PROCESO PARA ENVIO DE INFORMACIÓN A SERVIDOR DE CENTRALIZACION
//        $schedule->call(function() {
//            $cliente = new \GuzzleHttp\Client();
//            $res = $cliente->request('GET', 'http://192.168.0.9/api/v1/sincro/enviar/jfourside_adowa');
//            return $res->getBody();
//        })->dailyAt('12:55');

//PROCESO PARA ENVIO DE INFORMACIÓN A SERVIDOR DE INTEGRACION CON EL PROVEEDOR
        $schedule->call(function() {
            $cliente = new \GuzzleHttp\Client();
            $res = $cliente->request('POST', 'http://localhost/mas/public/integrador/enviar', [
                'body' => ['empresa' => 'Kimberly-Clark']
            ]);
            return $res->getBody()->getContents();
        })->dailyAt('17:05');
        
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }

}
