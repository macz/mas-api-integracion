<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendData extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:data {empresa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía los datos de integración hacia un servidor remoto sftp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $empresa = $this->argument('empresa');
        $this->line('Iniciando sincronización con servidor');
        try {
            $cliente = new \GuzzleHttp\Client();
            $params = [
                'empresa' => (string)$empresa
            ];
            $this->line($params);
            $res = $cliente->request('POST', 'http://localhost/mas/public/api/v1/integrador/enviar', [
                'form_params' => $params
            ]);
            $retorno = $res->getBody()->getContents();
            $this->info('Sincronización finalizada, INFO: ' . $retorno);
        } catch (Exception $e) {
            $this->error('Error de sincronización, ERROR: ' . $e->getMessage());
        }
    }

}
