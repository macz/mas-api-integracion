<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SincroData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sincro:data {database?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincroniza los datos de ventas, clientes e inventarios hacia un servidor central';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = $this->argument('database');
        
        $this->line('Iniciando sincronización con servidor');
        try {
        $cliente = new \GuzzleHttp\Client();
            $res = $cliente->request('GET', 'http://192.168.0.9/api/v1/sincro/enviar/'.$database);
            $retorno = $res->getBody();
            $this->info('Sincronización finalizada, INFO: '.$retorno);            
        } catch(Exception $e) {
            $this->error('Error de sincronización, ERROR: '.$e->getMessage());
        }
    }
}
