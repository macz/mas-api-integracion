<?php

namespace App\Http\Controllers;

class UpdateController extends Controller
{
    const VERSION_NAME = "1.1.0";
    const VERSION_CODE = 2;
    const URI = "http://192.168.1.101/mas/public/downloads/mas.apk";
    
    //Devuleve la ultima versión de la apk movil
    public function getLatestVersion()
    {
        $data = [
            'version_code' => self::VERSION_CODE,
            'version_name' => self::VERSION_NAME,
            'apk_uri' => self::URI,
            'success' => true
                ];
        return Response()->json($data, 200);
    }
}
