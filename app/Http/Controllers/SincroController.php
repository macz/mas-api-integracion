<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use App\Models\Servidor;
use Carbon\Carbon;
use App\Models\Inventario;
use App\Models\Cliente;
use App\Models\Venta;
use App\Models\Log;
use GuzzleHttp\Exception\ServerException;

class SincroController extends Controller {

    /**
     * SINCRONIZACIÓN DE PARTE DEL SERVIDOR SECUNDARIO
     * Esta área contiene las funciones para el envío de datos de sincronización
     * al servidor principal.
     */
    const DATABASE = 'jfourside';
    const SERVER_ID = '1';
    const USER = 'root';
    const PASS = 'root';
    const HOST = '192.168.1.101';
    const SERVER = 'http://192.168.1.101/mas/public/api/v1/sincro/';

    public function enviarData($database)
    {
        
        try {
            $this->setConfigDataBase($database);
            $serverID = $this->getServerID();
            $this->setDefaultDataBase();
            $this->escribiLog("Iniciando sincronización hacia servidor", "EVENTO", $serverID->Valor);            
            $this->setConfigDataBase($database);
            $fechaUltimaExportacion = $this->getFechaUltimaExportacionCliente($serverID);
            $data = array();
            $data['clientes'] = $this->generarDataClientes($fechaUltimaExportacion, $serverID);
            $data['inventarios'] = $this->generarDataInventarios($fechaUltimaExportacion, $serverID);
            $data['ventas'] = $this->generarDataVentas($fechaUltimaExportacion, $serverID);
            $this->setDefaultDataBase();
            $data['server_id'] = $serverID;
            $message = $this->sendData($data);
            if (strpos($message, 'success') > 0)
            {
                $code = 200;
            }
            else
            {
                $code = 400;
            }
        } catch (Exception $e) {
            $code = 400;
            $this->escribiLog("Error de sincronización: " . $code . ", " . $e->getMessage(), "ERROR", $serverID->Valor);
            $message = ['error' => $e->getMessage()];
        }
        $this->escribiLog("Finalizada la sincronización hacia servidor. Código: " . $code . ", " . $message, "EVENTO", $serverID->Valor);
        return Response()->json($message, $code);
    }

    private function sendData($data)
    {
        try {
        $cliente = new \GuzzleHttp\Client();
        $res = $cliente->request('POST', self::SERVER . 'guardar_data', [
            'json' => $data
        ]);
        } catch(ServerException $e) {
            return $e->getResponse()->getBody();
        }
        return $res->getBody()->getContents();
    }

    private function getFechaUltimaExportacionCliente($server)
    {
        $cliente = new \GuzzleHttp\Client();
        $res = $cliente->request('GET', self::SERVER . 'get_fecha_sincro/' . $server->Valor, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ]
        ]);
        $data = json_decode($res->getBody());
        return $data->fecha;
    }

    private function getServerID()
    {
        return DB::select("SELECT Valor FROM variables_sistema WHERE Clave='ID_SERVIDOR'")[0];
    }

    private function generarDataClientes($fechaUltimaExportacion, $serverID)
    {
        $query = "SELECT "
                . " cp.idcliente_profile AS cliente_id, "
                . $serverID->Valor . " AS servidor_id,"
                . " cp.Codigo AS codigo,"
                . " cp.Nombre_Comercial as nombre_comercial,"
                . " cp.Razon_Social as razon_social,"
                . " c.Nit as nit,"
                . " cp.Direccion_Completa as direccion,"
                . " c.DPI as dpi,"
                . " cp.Telefono_1 as telefono_1,"
                . " cp.Telefono_2 as telefono_2,"
                . " cp.Email as email,"
                . " cp.Activo as activo,"
                . " e.idEmpleados as vendedor_id,"
                . " e.Codigo as vendedor_codigo,"
                . " CONCAT(e.Nombre,' ',e.Apellido) as vendedor_nombre,"
                . " ru.idRutas as ruta_id,"
                . " ru.Nombre as ruta_codigo,"
                . " ru.Nombre as ruta_nombre,"
                . " cv.idcanales_ventas as canal_id,"
                . " cv.Nombre as canal_codigo,"
                . " cv.Nombre as canal_nombre,"
                . " ciu.idCiudad as ciudad_id,"
                . " ciu.Codigo as ciudad_codigo,"
                . " ciu.Nombre as ciudad_nombre,"
                . " reg.idRegiones as region_id,"
                . " reg.Codigo as region_codigo,"
                . " reg.Nombre as region_nombre,"
                . " mu.idMunicipios as municipio_id,"
                . " mu.Nombre as municipio_nombre,"
                . " mu.Codigo_Postal as municipio_codigo,"
                . " d.idDepartamento as departamento_id,"
                . " d.Nombre as departamento_nombre,"
                . " d.idDepartamento as departamento_codigo,"
                . " l.idListas_Precio as lista_id,"
                . " l.Nombre as lista_nombre,"
                . " tc.idtipo_cliente as tipo_id,"
                . " tc.Nombre as tipo_nombre"
                . " FROM cliente_profile cp "
                . " INNER JOIN clientes c ON cp.idcliente_profile=c.idClientes"
                . " INNER JOIN empleados e ON cp.Vendedor=e.idEmpleados"
                . " INNER JOIN listas_precio l ON l.idListas_Precio=cp.Lista_Precio"
                . " INNER JOIN tipo_cliente tc ON tc.idTipo_Cliente=cp.Tipo_Cliente"
                . " LEFT JOIN canales_ventas cv ON cv.idCanales_Ventas=cp.Canal"
                . " LEFT JOIN regiones reg ON reg.idRegiones=cp.Region"
                . " LEFT JOIN rutas ru ON ru.idRutas=cp.Ruta"
                . " LEFT JOIN ciudades ciu ON ciu.idCiudad=cp.Ciudad"
                . " LEFT JOIN municipios mu ON mu.idMunicipios=ciu.Municipio"
                . " LEFT JOIN departamentos d ON d.idDepartamento=mu.Departamento"
                . "  WHERE cp.Fecha_Actualizacion > :fechaUltimaExportacion";
        $clientes = DB::select(str_replace(':fechaUltimaExportacion', "'" . $fechaUltimaExportacion . "'", $query));
        return $clientes;
    }

    private function generarDataInventarios($fechaUltimaExportacion, $serverID)
    {
        $query = "SELECT "
                . $serverID->Valor . " AS servidor_id,"
                . "p.idProductos AS producto_id,
    p.Codigo AS codigo,
    p.Codigo_Proveedor AS codigo_proveedor,
    p.Nombre AS nombre,
    p.Proveedor AS proveedor_id,
    pro.Nit AS proveedor_codgio,
    pro.Nombre_Comercial AS proveedor_nombre,
    s.idSucursales AS sucursal_id,
    s.Codigo AS sucursal_codigo,
    s.Nombre_Comercial AS sucursal_nombre,
    b.idBodegas AS bodega_id,
    b.Nombre AS bodega_nombre,
    uni.idUnidades AS unidad_id,
    uni.Nombre AS unidad_nombre,
    uni.U_Base AS unidad_base,
    p.Costo_Bruto AS costo,
    p.Activo AS activo,
    SUM(pu.Existencia) AS existencia,
    p.Fecha_Ultima_Compra as ultima_compra
  FROM productos p
  INNER JOIN productos_ubicaciones pu ON pu.Productos=p.idProductos
  INNER JOIN ubicaciones u ON u.idUbicacion=pu.Ubicacion
  INNER JOIN bodegas b ON b.idBodegas=u.Bodega
  INNER JOIN proveedores pro ON pro.idProveedores=p.Proveedor
  INNER JOIN sucursales s ON s.idSucursales=b.Sucursal
  INNER JOIN unidades uni ON uni.idUnidades=p.Unidad_Base
   GROUP BY p.idProductos, b.idBodegas";
        $inventarios = DB::select($query);
        return $inventarios;
    }

    private function generarDataVentas($fechaUltimaExportacion, $serverID)
    {
        $query = "SELECT "
                . $serverID->Valor . " AS servidor_id,"
                . " p.idPedidos AS pedido_id,
                    l.idLinea_Detalle_Pedidos as linea_id,
    CONCAT(f.Serie,'-',f.Numero) AS factura,
    f.Fecha AS fecha,
    p.Hora_Inicio AS fecha_inicio,
    p.Hora_Finalizado AS fecha_fin,
    e.idEmpleados AS vendedor_id,
    e.Codigo AS vendedor_codigo,
    CONCAT(e.Nombre,' ',e.Apellido) AS vendedor_nombre,
    r.idRutas AS ruta_id,
    r.Nombre AS ruta_codigo,
    r.Nombre AS ruta_nombre,
    p.Estado AS estado,
    cl.idClientes AS cliente_id,
    cp.Codigo AS cliente_codigo,
    cl.Nombre AS cliente_nombre,
    cl.NIT AS cliente_nit,
    cl.Direccion AS cliente_direccion,
    l.No_Linea AS no_linea,
    l.Cantidad AS cantidad,
    l.Producto AS producto_id,
    pro.Codigo AS producto_codigo,
    pro.Nombre AS producto_nombre,
    IFNULL(l.Detalle,pro.Nombre) AS detalle,
    unis.idUnidades AS unidad_id,
    unis.Nombre AS unidad_nombre,
    unis.U_Base AS unidad_base,
    l.Precio as precio,
    l.Costo as costo,
    l.Descuento as descuento,
    lis.idListas_Precio AS lista_id,
    lis.nombre AS lista_nombre
FROM linea_detalle_pedidos l
INNER JOIN pedidos p ON l.pedido=p.idPedidos
INNER JOIN facturas f ON f.idFacturas=l.Factura
INNER JOIN empleados e ON e.idEmpleados=p.Vendedor
INNER JOIN clientes cl ON cl.idClientes=p.Cliente
LEFT JOIN listas_precio lis ON lis.idListas_Precio=l.Lista_Precio
LEFT JOIN unidades unis ON unis.idUnidades=l.Unidad
LEFT JOIN productos pro ON pro.idProductos=l.Producto
LEFT JOIN cliente_profile cp ON cp.idcliente_profile=cl.idClientes
LEFT JOIN rutas r ON r.idRutas=p.Ruta
WHERE p.Hora_Finalizado > :fechaUltimaExportacion
GROUP BY l.idLinea_Detalle_Pedidos";
        $ventas = DB::select(str_replace(':fechaUltimaExportacion', "'" . $fechaUltimaExportacion . "'", $query));
        return $ventas;
    }

    private function setConfigDataBase($database)
    {
        Config::set('database.connections.tenant.host', self::HOST);
        Config::set('database.connections.tenant.username', self::USER);
        Config::set('database.connections.tenant.password', self::PASS);
        Config::set('database.connections.tenant.database', ($database) ? $database : self::DATABASE);

        Config::set('database.default', 'tenant');
        DB::purge('tenant');
        DB::reconnect('tenant');
    }

    private function setDefaultDataBase()
    {
        Config::set('database.default', 'mysql');
        DB::purge('mysql');
        DB::reconnect('mysql');
    }

    /**
     * SINCRONIZACIÓN DE PARTE DEL SERVIDOR PRINCIPAL
     * Esta área contiene las funciones para la recepción de datos desde el servidor
     * secundario hacial el principal.
     */
    public function getFechaUltimaExportacionServer($server_id)
    {
        try {
            $serverID = $server_id;
            if ($serverID)
            {
                $fecha = DB::select("SELECT ultima_sincronizacion AS ultima FROM servidores WHERE servidor_id=" . $serverID);
                $code = 200;
                if ($fecha)
                {
                    $data = ['fecha' => $fecha[0]->ultima];
                }
                else
                {
                    $data = ['fecha' => "2010-01-01 00:00:00"];
                }
            }
            else
            {
                $code = 400;
                $data = ['error' => 'Servidor no especificado'];
                $this->escribiLog("Error no se especifico servidor para últma fecha de exportación: " . $code, "ERROR", $serverID);
            }
        } catch (Exception $e) {
            $code = 400;
            $data = ['error' => $e->getMessage()];
            $this->escribiLog("Error no se pudo generar fecha de última exportación: " . $code . ", " . $e->getMessage(), "ERROR", $serverID);
        } finally {
            return Response()->json($data, $code);
        }
    }

    public function guardarData(Request $request)
    {
        try {
            $data = array();
            $code = 200;
            $clientes = $request->input('clientes');
            $this->guardarClientes($clientes);
            $inventarios = $request->input('inventarios');
            $this->guardarInventarios($inventarios);
            $ventas = $request->input('ventas');
            $this->guardarVentas($ventas);
            $server = $request->input('server_id');
            $this->actualizarServer($server);
            $data = ['message' => 'Datos guardados con exito', 'success' => 1];
        } catch (Exception $e) {
            $code = 400;
            $data = ['error' => $e->getMessage(), 'success' => 0];
            $this->escribiLog("Error no se pudo guardar la información desde cliente: " . $code . ", " . $e->getMessage(), "ERROR", $server);
        } finally {
            return Response()->json($data, $code);
        }
    }

    private function guardarInventarios($inventarios)
    {
        if ($inventarios)
        {
            foreach ($inventarios as $inv)
            {
                $inv = (array)$inv;
                $inventario = Inventario::where('servidor_id', $inv['servidor_id'])
                        ->where('producto_id', $inv['producto_id'])
                        ->where('bodega_id', $inv['bodega_id'])
                        ->first();
                if ($inventario)
                {
                    $inventario->fill($inv);
                    $inventario->save();
                }
                else
                {
                    $inventario = new Inventario();
                    $inventario->fill($inv);
                    $inventario->save();
                }
            }
        }
    }

    private function guardarClientes($clientes)
    {
        if ($clientes)
        {
            foreach ($clientes as $cli)
            {
                $cli = (array)$cli;
                $cliente = Cliente::where('servidor_id', $cli['servidor_id'])
                        ->where('cliente_id', $cli['cliente_id'])
                        ->first();
                if ($cliente)
                {
                    $cliente->fill($cli);
                    $cliente->save();
                }
                else
                {
                    $cliente = new Cliente();
                    $cliente->fill($cli);
                    $cliente->save();
                }
            }
        }
    }

    private function guardarVentas($ventas)
    {
        if ($ventas)
        {
            foreach ($ventas as $vent)
            {
                $vent = (array)$vent;
                $venta = Venta::where('servidor_id', $vent['servidor_id'])
                        ->where('pedido_id', $vent['pedido_id'])
                        ->where('linea_id', $vent['linea_id'])
                        ->first();
                if ($venta)
                {
                    $venta->fill($vent);
                    $venta->save();
                }
                else
                {
                    $venta = new Venta();
                    $venta->fill($vent);
                    $venta->save();
                }
            }
        }
    }

    private function actualizarServer($server)
    {
        $servidor = Servidor::where('servidor_id', $server)->first();
        if ($servidor)
        {
            $servidor->ultima_sincronizacion = Carbon::now();
            $servidor->save();
        }
    }

    /*
     *  FUNCIÓN GENERAL PARA ESCRIBIR EN EL LOG DE ACTIVIDADES
     */

    private function escribiLog($evento, $tipo, $cliente_id)
    {
        $log = new Log();
        $log->evento = $evento;
        $log->tipo = $tipo;
        $log->cliente_id = $cliente_id;
        $log->servidor_id = self::SERVER_ID;
        $log->save();
    }

}
