<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ZipArchive;
use Exception;

class IntegradorController extends Controller {

    private $STORAGE_DIR;
    private $CONFIG_DIR;

    public function index()
    {
        $this->CONFIG_DIR = storage_path() . '/app/public/xml/';
        return view("integrador/index", ['empresas' => $this->getEmpresas()]);
    }

    public function configurar($empresa = null)
    {
        $this->CONFIG_DIR = storage_path() . '/app/public/xml/';
        $empresas = $this->getEmpresas();
        if (!$empresa)
        {
            $empresa = array_values($empresas)[0];
        }
        $config = $this->getDataConfigFromXML($this->CONFIG_DIR . 'config.xml', $empresa);
        $data = [
            'empresas' => $empresas,
            'config' => $config,
            'consultas' => $config['consultas'],
            'empresa' => $empresa
        ];
        return view("integrador/configuration", $data);
    }

    public function guardarConfiguracion(Request $request)
    {
        $this->CONFIG_DIR = storage_path() . '/app/public/xml/';
        $arrEmpresasXML = simplexml_load_file($this->CONFIG_DIR . 'config.xml');
        foreach ($arrEmpresasXML as $config)
        {
            if ($config->nombre == $request->input('empresa'))
            {
                $config->archivoPatron = $request->input('patron');
                $config->archivoCaracterSeparacion = $request->input('caracterSeparacion');
                $config->ZIPPatron = $request->input('patronZip');
                $config->archivoEncabezado = $request->input('encabezado') == 'TRUE' ? 'TRUE' : 'FALSE';
                $config->ZIP = $request->input('zip') == 'TRUE' ? 'TRUE' : 'FALSE';
                $config->ftpServidor = $request->input('ftpServer');
                $config->ftpUsuario = $request->input('ftpUser');
                if ($request->input('ftpPass'))
                {
                    $config->ftpContrasena = $request->input('ftpPass');
                }
                $config->ftpDirectorio = $request->input('ftpDir');
                $config->dbServidor = $request->input('dbServer');
                $config->dbUsuario = $request->input('dbUser');
                if ($request->input('dbPass'))
                {
                    $config->dbContrasena = $request->input('dbPass');
                }
                $config->dbNombre = $request->input('dbName');
                foreach ($config->consulta as $consulta)
                {
                    $consulta->query = $request->input('query_' . $consulta->nombre);
                    $datetime = \Carbon\Carbon::parse($request->input('fechaultima_' . $consulta->nombre))->format('Y-m-d H:i:s');
                    $consulta->fechaUltimaExportacion = $datetime;
                }
            }
        }
        $arrEmpresasXML->asXml($this->CONFIG_DIR . 'config.xml');
        \Illuminate\Support\Facades\Session::flash('alert', 'Configuración guardada con éxito');
        return redirect()->route('configurar_integrador', $request->input('empresa'));
    }

    public function enviar(Request $request)
    {
        $this->STORAGE_DIR = storage_path() . '/app/public/storage/';
        $this->CONFIG_DIR = storage_path() . '/app/public/xml/';
        $api = $request->is('api*');
        //Obteniendo variables - parametros
        $empresa = $request->input('empresa');
        
        //$consultas = $request->input('consultas');
        //$user = $request->input('user');
        //$pass = $request->input('pass');
        $fechaExportacion = date("Y-m-d H:i:s");

        //Parametros de Prueba
        // $empresa = "Kelloggs";
        // $consultas = "ALL";
        $user = "INTEGRADOR";
        $consultas = "ALL";
        $pass = "81dc9bdb52d04dc20036dbd8313ed055";
        //PROGRAMA PRINCIPAL MAIN
        try {
            //Obtenemos la configuración del archivo XML en un array
            $config = $this->getDataConfigFromXML($this->CONFIG_DIR . 'config.xml', $empresa);

            $this->setConfigDataBase($config);

            //Validamos los datos de usuario
            if ($this->validarUsuario($user, $pass))
            {
                //Escribios los archivos con el resultado de las consultas requeridas
                $this->writeFilesFromQuerys($config, $consultas, $fechaExportacion);

                //Obteniendo lista de archivos creados
                $files = $this->getFilesInDir($this->STORAGE_DIR . $config["localDirectorio"]);

                $zip = ($config["ZIP"] == "TRUE" ? 1 : 0 );
                if ($zip)
                {
                    $nombreArchivo = $this->createZip($files, $config, $fechaExportacion);
                    //$this->subirSFTP($config, $nombreArchivo);
                }
                else
                {
                    foreach ($files as $name => $fileDir)
                    {
                        //Quitando el localdirectorio para obtener solo el nombre del archivo
                        $file = str_replace($this->STORAGE_DIR . $config["localDirectorio"] . "/", "", $fileDir);
                        //$this->subirSFTP($config, $file);
                    }
                }

                $archivo = $this->backupFiles($config, $fechaExportacion);
                $archivo = asset('storage/storage/' . $config["localDirectorio"] . '/Backup/' . $archivo);
                $this->deleteFiles($files);
                $this->restoreConfig();
                $this->setDefaultDataBase();
                $archivos = array();
                foreach ($files as $name => $fileDir)
                {
                    array_push($archivos, str_replace($this->STORAGE_DIR . $config["localDirectorio"] . "/", "", $fileDir));
                }
                \Illuminate\Support\Facades\Session::flash('alert', 'Envío de archivos ejecutado con éxito');
                if (!$api)
                {
                    return view('integrador/success', ['backup' => $archivo, 'archivos' => $archivos, 'empresa' => $empresa]);
                }
                else
                {
                    return Response()->json(['backup' => $archivo, 'archivos' => $archivos, 'empresa' => $empresa, 'success' => 1]);
                }
            }
            else
            {
                $this->setDefaultDataBase();
                if (!$api)
                {
                    return \Illuminate\Support\Facades\Redirect::route('integrador')->withErrors("Error: Usuario no autenticado");
                }
                else
                {
                    return Response()->json(['Error' => 'Usuario no autenticado']);
                }
            }
        } catch (Exception $e) {
            if (isset($files))
            {
                $this->deleteFiles($files);
            }
            $this->setDefaultDataBase();
            if (!$api)
            {
                return \Illuminate\Support\Facades\Redirect::route('integrador')->withErrors("Error: " . $e->getMessage());
            }
            else
            {
                return Response()->json(['Error' => 'Error: ' . $e->getMessage()]);
            }
        }
    }

    private function setConfigDataBase($config)
    {
        \Illuminate\Support\Facades\Config::set('database.connections.tenant.host', $config['dbServidor']);
        \Illuminate\Support\Facades\Config::set('database.connections.tenant.username', $config['dbUsuario']);
        \Illuminate\Support\Facades\Config::set('database.connections.tenant.password', $config['dbContrasena']);
        \Illuminate\Support\Facades\Config::set('database.connections.tenant.database', $config['dbNombre']);

        \Illuminate\Support\Facades\Config::set('database.default', 'tenant');
        DB::purge('tenant');
        DB::reconnect('tenant');
    }

    private function setDefaultDataBase()
    {
        \Illuminate\Support\Facades\Config::set('database.default', 'mysql');
        DB::purge('mysql');
        DB::reconnect('mysql');
    }

    private function nombreArchivo($patron, $datos, $fechaExportacionFuncion)
    {
        $datos += [
            "pais" => "GTM",
            "dia" => substr($fechaExportacionFuncion, 8, 2),
            "mes" => substr($fechaExportacionFuncion, 5, 2),
            "anio" => substr($fechaExportacionFuncion, 0, 4),
            "horas" => substr($fechaExportacionFuncion, 11, 2),
            "minutos" => substr($fechaExportacionFuncion, 14, 2),
            "segundos" => substr($fechaExportacionFuncion, 17, 2)
        ];

        $cadena = $patron;
        foreach ($datos as $clave => $valor)
        {
            $cadena = str_replace("%" . $clave . "%", $valor, $cadena);
        }

        return $cadena;
    }

    private function actualizaFechaUltimaExportacion($cuentaActualizaciones, $iempresa, $iconsulta, $fechaExportacionFuncion)
    {
        //debido a que las actualizaciones se hacen consulta por consulta, es necesario hacer la primera
        //basada en config, y las demas basadas en config_temp para que se completen una tras otra
        if ($cuentaActualizaciones == 1)
        {
            $arrEmpresasXML = simplexml_load_file($this->CONFIG_DIR . 'config.xml');
        }
        else
        {
            $arrEmpresasXML = simplexml_load_file($this->CONFIG_DIR . 'config_temp.xml');
        }
        $arrEmpresasXML->empresa[$iempresa]->consulta[$iconsulta]->fechaUltimaExportacion = $fechaExportacionFuncion;
        $arrEmpresasXML->asXml($this->CONFIG_DIR . 'config_temp.xml');

        $cuentaActualizaciones++;
        return $cuentaActualizaciones;
    }

    private function getDataConfigFromXML($fileXML, $empr)
    {
        $arrEmpresas = simplexml_load_file($fileXML);
        if (!$arrEmpresas)
        {
            throw new Exception('Error en la carga de archivo XML de configuracion: ' . $fileXML);
        }
        $arrConexiones = array();
        $iempresa = 0;
        foreach ($arrEmpresas as $empresa)
        {
            if ($empresa->nombre == $empr)
            {
                array_push($arrConexiones, [
                    "empresa" => (string)$empresa->nombre,
                    "dbNombre" => (string)$empresa->dbNombre,
                    "dbServidor" => (string)$empresa->dbServidor,
                    "dbUsuario" => (string)$empresa->dbUsuario,
                    "dbContrasena" => (string)$empresa->dbContrasena,
                    "localDirectorio" => (string)$empresa->localDirectorio,
                    "archivoCaracterSeparacion" => (string)$empresa->archivoCaracterSeparacion,
                    "archivoEncabezado" => (string)$empresa->archivoEncabezado,
                    "archivoPatron" => (string)$empresa->archivoPatron,
                    "iempresa" => $iempresa,
                    "ZIP" => (string)$empresa->ZIP,
                    "ZIPPatron" => (string)$empresa->ZIPPatron,
                    "consultas" => $empresa->consulta,
                    "ftpServidor" => (string)$empresa->ftpServidor,
                    "ftpUsuario" => (string)$empresa->ftpUsuario,
                    "ftpContrasena" => (string)$empresa->ftpContrasena,
                    "ftpDirectorio" => (string)$empresa->ftpDirectorio
                ]);
            }
            $iempresa++;
        }
        if (empty($arrConexiones))
        {
            throw new Exception("La empresa " . $empresa . " no esta definida en la configuración");
        }
        return $arrConexiones[0];
    }

    private function validarUsuario($user, $pass)
    {
        return true;
        $sent = "SELECT u.contrasena as contrasena, r.Nombre AS rol
                                        FROM usuarios AS u, roles AS r
                                        WHERE u.Rol = r.idRoles AND u.Nombre = :usuario";
        $result = DB::select($sent, array('usuario' => $user));
        if (!$result)
        {
            return false;
        }
        foreach ($result as $r)
        {
            if ($r->contrasena != $pass /* OR $r->rol != "INTEGRADOR" */)
            {
                return false;
            }
        }
        return true;
    }

    private function writeEncabezado($file, $config, $sent)
    {
        $archivoEncabezado = (string)$config["archivoEncabezado"];
        $archivoCaracterSeparacion = (string)$config["archivoCaracterSeparacion"];
        if ($archivoEncabezado == "TRUE")
        { //verifica si es requerido el encabezado
            $cabecera = "";
            $collect = collect($sent[0]);
            foreach ($collect->keys() as $columna)
            {
                $cabecera .= $columna . $archivoCaracterSeparacion;
            }
            $cabecera = substr($cabecera, 0, strlen($cabecera) - 1);
            fwrite($file, $cabecera . "\r\n");
        }
    }

    private function writeContenido($file, $config, $sent)
    {
        $archivoCaracterSeparacion = (string)$config["archivoCaracterSeparacion"];
        foreach ($sent as $row)
        {//escribe cada linea de la query en el archivo
            $cadena = "";
            foreach ($row as $column)
            {
                $cadena .= $column . $archivoCaracterSeparacion;
            }
            $cadena = substr($cadena, 0, strlen($cadena) - 1);
            fwrite($file, $cadena . "\r\n");
        }
    }

    private function deleteEmptyFile($nombre, $directorio)
    {
        if (filesize($directorio . '/' . $nombre) <= 0)
        {
            unlink($directorio . '/' . $nombre);
        }
    }

    private function subirFTP($config, $nombreArchivo)
    {
        //Iniciando variables
        $ftpServidor = (string)$config["ftpServidor"];
        $ftpDirectorio = (string)$config["ftpDirectorio"];
        $ftpUsuario = (string)$config["ftpUsuario"];
        $ftpContrasena = (string)$config["ftpContrasena"];
        $localDirectorio = $this->STORAGE_DIR . ((string)$config["localDirectorio"]) . '/';
        $ftpArchivo = $ftpDirectorio . '/' . $nombreArchivo;
        $localArchivo = $localDirectorio . $nombreArchivo;
        set_time_limit(60); //reiniciando el tiempo límite de PHP para que dé tiempo de subir archivos al FTP
        //Subiendo el archivo al FTP
        $idConexion = ftp_connect($ftpServidor);
        $login = ftp_login($idConexion, $ftpUsuario, $ftpContrasena);
        ftp_pasv($idConexion, true);

        $varResultFuncion = '';

        if ((!$idConexion) or ( !$login))
        {
            throw new Exception("Fallo la conexion al servidor FTP");
        }

        $upload = ftp_put($idConexion, $ftpArchivo, $localArchivo, FTP_BINARY);

        if (!$upload)
        {
            throw new Exception("Fallo la carga del archivo mientras se subia al servidor FTP");
        }

        ftp_close($idConexion);
    }

    private function subirSFTP($config, $nombreArchivo)
    {
        //Iniciando variables
        $ftpServidor = (string)$config["ftpServidor"];
        $ftpDirectorio = (string)$config["ftpDirectorio"];
        $ftpUsuario = (string)$config["ftpUsuario"];
        $ftpContrasena = (string)$config["ftpContrasena"];
        $localDirectorio = $this->STORAGE_DIR . ((string)$config["localDirectorio"]) . '/';
        $ftpArchivo = $ftpDirectorio . $nombreArchivo;
        $localArchivo = $localDirectorio . $nombreArchivo;
        foreach ($config["consultas"] as $consulta)
        {
            if (strpos($nombreArchivo, (string)$consulta->nombre) !== false)
            {
                $subdir = (string)$consulta->ftpDirectorio;
                if ($subdir)
                {
                    $ftpArchivo = $ftpDirectorio . $subdir . "/" . $nombreArchivo;
                }
            }
        }
        //Subiendo el archivo al FTP
        $idConexion = ssh2_connect($ftpServidor);
        $login = ssh2_auth_password($idConexion, $ftpUsuario, $ftpContrasena);
        $sftp = ssh2_sftp($idConexion);

        if ((!$idConexion) or ( !$login))
        {
            throw new Exception("Fallo la conexion al servidor FTP");
        }

        $content = file_get_contents($localArchivo);
        file_put_contents("ssh2.sftp://" . intval($sftp) . "/{$ftpArchivo}", $content);
    }

    private function writeFilesFromQuerys($config, $consu, $fechaExportacion)
    {
        $cuentaActualizaciones = 1; //(en la funcion actualizaFechaUltimaExportacion) Sirve para contar
        //las veces que se actualiza el config_temp, si es la primera vez se toma de config si no es la primera vez
        //los toma de config_temp porque se va actualizando fecha por fecha de cada consulta
        //Indice de la consulta actual dentro del arreglo de consultas
        $iconsulta = 0;
        foreach ($config["consultas"] as $consulta)
        {
            if ($consu == (string)$consulta->nombre OR $consu == 'ALL')
            {

                //Inicializando variables necesarias para crear y escribir el archivo
                $localDirectorio = $this->STORAGE_DIR . ((string)$config["localDirectorio"]);
                $archivoPatron = (string)$config["archivoPatron"];
                $datos = [
                    "consulta" => (string)$consulta->nombre
                ];
                //Generamos el nombre del archivo a partir del patrón de configuración
                $nombreArchivo = $this->nombreArchivo($archivoPatron, $datos, $fechaExportacion);

                //Creando el nombre del archivo enviando $datos con la consulta + la empresa
                if (!is_dir($localDirectorio))
                {
                    mkdir($localDirectorio);
                }

                //Se genera la consulta y se escribe su contenido en el archivo
                $sent = DB::select(str_replace(':fechaUltimaExportacion', "'" . $consulta->fechaUltimaExportacion . "'", (string)$consulta->query));
                
                if ($sent)
                {
                    //Abrimos el archivo
                    $archivo = fopen($localDirectorio . '/' . $nombreArchivo, "w");

                    //Escribimos los encabezados si es necesario
                    $this->writeEncabezado($archivo, $config, $sent);

                    //Escribimos el contenido del archivo
                    $this->writeContenido($archivo, $config, $sent);

                    //Cerramos el archivo
                    fclose($archivo);

                    //Borramos el archivo si esta vacío
                    $this->deleteEmptyFile($nombreArchivo, $localDirectorio);

                    //Actualizando la fechaUltimaExportacion correspondiente en un archivo temporal
                    $cuentaActualizaciones = $this->actualizaFechaUltimaExportacion($cuentaActualizaciones, $config["iempresa"], $iconsulta, $fechaExportacion);
                }
            }
            $iconsulta++;
        }
    }

    private function deleteFiles($files)
    {
        foreach ($files as $name => $fileDir)
        {
            if (file_exists($fileDir))
            {
                unlink($fileDir);
            }
        }
    }

    private function getFilesInDir($dir)
    {
        //Obteniendo la lista de archivos creados en el directorio especificado
        $files = array();
        foreach (glob($dir . '/' . "*.TXT") as $file)
        {
            $files[] = $file;
        }
        return $files;
    }

    private function createZip($files, $config, $fechaExportacion)
    {
        $localDirectorio = ($this->STORAGE_DIR . (string)$config["localDirectorio"]) . '/';
        $patron = (string)$config["ZIPPatron"];
        //Creando el zip y metiendo lo txt creados. El nombre es en un formato dado
        $nombreArchivo = $this->nombreArchivo($patron, array(), $fechaExportacion);
        $zip = new ZipArchive;
        if ($zip->open($localDirectorio . $nombreArchivo, ZipArchive::CREATE) === TRUE)
        {
            foreach ($files as $name => $fileDir)
            {
                //Quitando el localdirectorio para obtener solo el nombre del archivo
                $file = str_replace($localDirectorio, "", $fileDir);
                // Add current file to archive
                $zip->addFile($fileDir, $file);
            }
            $zip->close();
        }
        else
        {
            throw new Exception("Fallo la creacion del archizo ZIP");
        }
        return $nombreArchivo;
    }

    private function restoreConfig()
    {
        //Actualizando el config oficial con el temp que tiene las fechas de actualizacion
        $arrEmpresas = simplexml_load_file($this->CONFIG_DIR . 'config_temp.xml');
        $arrEmpresas->asXml($this->CONFIG_DIR . 'config.xml');
    }

    private function backupFiles($config, $fechaExportacion)
    {
        $localDirectorio = $this->STORAGE_DIR . ((string)$config["localDirectorio"]) . '/';
        if (is_dir($localDirectorio))
        {
            $patron = (string)$config["ZIPPatron"];
            //Creando el zip y metiendo lo txt creados. El nombre es en un formato dado
            $nombreArchivo = $this->nombreArchivo($patron, array(), $fechaExportacion);
            $zip = ($config["ZIP"] == "TRUE" ? 1 : 0 );
            if (!is_dir($localDirectorio . 'Backup'))
            {
                mkdir($localDirectorio . 'Backup');
            }

            if ($zip)
            {
                //Si todo marcha con normalidad se moverá el ZIP a la carpeta BackUp
                rename($localDirectorio . $nombreArchivo, $localDirectorio . "Backup/" . $nombreArchivo);
            }
            else
            {
                $files = $this->getFilesInDir($localDirectorio);
                $nombreArchivo = $this->createZip($files, $config, $fechaExportacion);
                rename($localDirectorio . $nombreArchivo, $localDirectorio . "Backup/" . $nombreArchivo);
            }
            return $nombreArchivo;
        }
    }

    private function getEmpresas()
    {
        $arrEmpresas = simplexml_load_file($this->CONFIG_DIR . 'config.xml');
        if (!$arrEmpresas)
        {
            throw new Exception('Error en la carga de archivo XML de configuracion ');
        }
        $empresas = array();
        foreach ($arrEmpresas as $empresa)
        {
            $empresas[(string)$empresa->nombre] = (string)$empresa->nombre;
        }
        return $empresas;
    }

}
