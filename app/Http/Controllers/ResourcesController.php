<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

/* TODO en esta clase se debe refactorizar las consultas para utilizar 
 *  el ORM de laravel en lugar de usar consultas crudas 
 */

class ResourcesController extends Controller {

    /* TODO Valida si el usuario existe, esto necesitarà refactorizarse como
     *  seguridad a nivel de API
     */
    
    const DATABASE = 'jfourside_adowa';
    const USER = 'root';
    const PASS = 'nerger7726';
    const HOST = '192.168.0.9';

    private function validateUser($id)
    {
        $valid = false;
        $users = DB::select("SELECT idUsuarios FROM usuarios WHERE idUsuarios=" . $id);

        if ($users)
        {
            $valid = true;
        }
        return $valid;
    }

    //Ejecuta la query de selección cruda
    private function executeQuery($query)
    {
        $data = DB::select($query);
        return $data;
    }

    //Valida al usuario y devuelve un array con los datos de la consulta
    private function generateData($query, $usuario, $query2 = NULL)
    {
        $this->setConfigDataBase(self::DATABASE);
        if ($this->validateUser($usuario))
        {
            $data = $this->executeQuery($query);
            $server_response["data"] = $data;
            if ($query2)
            {
                $data2 = $this->executeQuery($query2);
                $server_response["data2"] = $data2;
            }
            $server_response["success"] = 1;
            if (!$data)
            {
                $server_response["message"] = "No se encontraron registros";
            }
        }
        else
        {
            $server_response["success"] = 0;
            $server_response["message"] = "Usuario no autorizado";
        }
        $this->setDefaultDataBase();
        return $server_response;
    }

    //Devuleve json con listado de agendas
    public function getAgendas(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idvisitas as id, cliente, fecha, vendedor, motivo_anula as motivoanula "
                . "FROM visitas "
                . "WHERE bodega="
                . $bodega
                . " AND vendedor="
                . $usuario . " AND estado='PLAN'";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de bodegas
    public function getBodegas(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idBodegas as id, Nombre as nombre FROM bodegas";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de categorias de productos
    public function getCategorias(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idCategorias as id, Nombre as nombre FROM categorias";
        $query2 = "SELECT idCategorias as categoria, idProductos as producto FROM producto_categorias";
        $server_response = $this->generateData($query, $usuario, $query2);
        return $server_response;
    }

    //Devuleve json con listado de clientes
    public function getClientes(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT c.idClientes as id, c.Nombre as nombre, p.Codigo as codigo, c.NIT as nit, c.Direccion as direccion, p.Activo as activo, p.Lista_Precio as listaprecio, p.Tipo_cliente as tipocliente "
                . "FROM clientes c INNER JOIN cliente_profile p ON c.idClientes=p.idcliente_profile "
                . "WHERE p.Vendedor=" . $usuario . " AND p.Activo=1 "
                . "GROUP BY c.idClientes ";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de detalle de ofertas
    public function getDetalleOfertas(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT iddetalle_oferta as id, d.Oferta as oferta, d.Producto as producto, d.Precio as precio, d.Cantidad as cantidad, d.Unidad as unidad FROM detalle_oferta d INNER JOIN ofertas o ON d.Oferta=o.idOfertas WHERE o.Bodega="
                . $bodega;
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de listas de precio
    public function getListasPrecio(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idListas_Precio as id, Nombre as nombre FROM listas_precio";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de ofertas
    public function getOfertas(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idOfertas as id, Nombre, Descripcion, Precio, Bodega FROM ofertas WHERE Bodega=" . $bodega;
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de precios
    public function getPrecios(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT Producto, Unidad, Listas_Precio, Precio FROM precios_productos";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de productos
    public function getProductos(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT p.idProductos as id, p.Nombre as nombre, m.Nombre as marca, p.Codigo as codigo, pub.Existencia as existencia, p.Existencia_Minima as existencia_minima "
                . "FROM productos p INNER JOIN marcas m ON p.marca=m.idMarcas "
                . "INNER JOIN productos_ubicaciones pub ON pub.Productos=p.idProductos "
                . "INNER JOIN ubicaciones u ON pub.Ubicacion=u.idUbicacion "
                . "WHERE u.Bodega=" . $bodega . " AND pub.Defecto=1 AND p.Activo=1";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de tipo clientes
    public function getTipoClientes(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idtipo_cliente, Nombre FROM tipo_cliente";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de unidades
    public function getUnidades(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT idUnidades, Nombre, Abreviatura, U_Base FROM unidades";
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    //Devuleve json con listado de usuarios
    public function getUsuarios(Request $request)
    {
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $query = "SELECT "
                . "u.idUsuarios as id, u.Nombre as nombre, u.Contrasena as contrasena, u.bodega as bodega, e.Comision as comision, e.idEmpleados as vendedor "
                . "FROM empleados e INNER JOIN usuarios u ON e.idEmpleados=u.idUsuarios "
                . "WHERE u.bodega=" . $bodega;
        $server_response = $this->generateData($query, $usuario);
        return $server_response;
    }

    private function getRuta($cliente)
    {
        return DB::table('cliente_profile')
                        ->where('idcliente_profile', $cliente)
                        ->pluck('ruta');
    }

    private function getDescripcion($idProducto)
    {
        return DB::table('productos')
                        ->where('idProductos', $idProducto)
                        ->pluck('Nombre');
    }

    private function getListaPrecio($idProducto, $precio, $cliente)
    {
        $lista = DB::table('precios_productos')
                ->where('Producto', $idProducto)
                ->where('Precio', $precio)
                ->pluck('Listas_Precio');
        if (!$lista)
        {
            $lista = DB::table('cliente_profile')
                    ->where('idcliente_profile', $cliente)
                    ->pluck('Lista_Precio');
        }
        return $lista;
    }

    private function savePedido($pedido)
    {
        $pedidoquery = "INSERT INTO pedidos (Fecha, Cliente, Vendedor, Total_Lineas,"
                . " Hora_Inicio, Sub_Total,"
                . " Descuento, Total, Observaciones, Estado,"
                . " Bodega, Usuario, Terminal, Ruta)"
                . " VALUES (:fecha,:cliente,:vendedor,:total_lineas,:hora_inicio,"
                . ":subtotal,:descuento,:total,:observaciones,'En Proceso',:bodega,:usuario,:terminal,:ruta)";
        $pedidoparams = array();
        foreach ($pedido as $nombre => $param)
        {
            if (($nombre != "detalles") && ($nombre != "agenda"))
            {
                $pedidoparams[$nombre] = $param;
            }
        }
        $pedidoparams["ruta"] = $this->getRuta($pedido["cliente"])->first();
        $pedidoparams["terminal"] = 35;
        DB::insert($pedidoquery, $pedidoparams);
        $resp = DB::getPdo()->lastInsertId();
        if (isset($pedido["agenda"]))
        {
            $agenda = $pedido["agenda"];
            $this->saveAgenda($resp, $agenda);
        }
        return $resp;
    }

    private function saveAgenda($pedido, $agenda)
    {
        $agendaquery = "UPDATE visitas SET hora_inicio=:horainicio, hora_fin=:horafin, estado=:estado, pedido=:pedido WHERE idvisitas=:visita";
        $agendaparams = array();
        $agendaparams["pedido"] = $pedido;
        foreach ($agenda as $nombre => $param)
        {
            $agendaparams[$nombre] = $param;
        }
        DB::update($agendaquery, $agendaparams);
    }

    private function saveDetalle($pedido, $id)
    {
        $detallesquery = "INSERT INTO linea_detalle_pedidos (Pedido, Producto, Cantidad, Precio, Unidad, Descuento, Secuencia_Repeticion, No_Linea, Detalle, Lista_Precio)"
                . " VALUES (:pedido,:producto,:cantidad,:precio,:unidad,:descuento,1,:nolinea,:detalle,:lista)";
        $detallesparams = array();
        $detallesparams["pedido"] = $id;
        $linea = 1;
        foreach ($pedido["detalles"] as $detalle)
        {
            foreach ($detalle as $nombre => $param)
            {
                if ($nombre != "precioUnitario")
                {
                    $detallesparams[$nombre] = $param;
                }
            }
            $detallesparams["nolinea"] = $linea;
            $detallesparams["detalle"] = $this->getDescripcion($detalle["producto"])->first();
            $detallesparams["lista"] = $this->getListaPrecio($detalle["producto"], $detalle["precioUnitario"], $pedido["cliente"])->first();
            DB::insert($detallesquery, $detallesparams);
            $linea++;
        }
    }

    //Guarda los datos de los pedidos sincronizados
    public function persistPedidos(Request $request)
    {
        $this->setConfigDataBase(self::DATABASE);
        $usuario = $request->input("usuario");
        $bodega = $request->input("bodega");
        $datapedidos = $request->input("data");
        $jsondata = json_decode($datapedidos, true);
        DB::beginTransaction();
        try {
            foreach ($jsondata as $pedido)
            {
                $resp = $this->savePedido($pedido);
                if ($resp) {
                    $this->saveDetalle($pedido, $resp);
                } else {
                    throw new Exception("Error no se pudo guardar pedido");
                }
            }
            DB::commit();
            return array("SUCCESS"=>"SINCRONIZACIÓN EXITOSA");
        } catch (Exception $e) {
            DB::rollback();
            return array("ERROR"=>"ERROR :"+$e->getMessage());
        }
        $this->setDefaultDataBase();
    }
    
     private function setConfigDataBase($database)
    {
        Config::set('database.connections.tenant.host', self::HOST);
        Config::set('database.connections.tenant.username', self::USER);
        Config::set('database.connections.tenant.password', self::PASS);
        Config::set('database.connections.tenant.database', ($database) ? $database : self::DATABASE);

        Config::set('database.default', 'tenant');
        DB::purge('tenant');
        DB::reconnect('tenant');
    }

    private function setDefaultDataBase()
    {
        Config::set('database.default', 'mysql');
        DB::purge('mysql');
        DB::reconnect('mysql');
    }

}
