<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MasWebController extends Controller
{
    const API_URL = "https://api.deleon.com.gt";
    public $database = "cdl";

    //const API_URL = "localhost:8000";

    private function getUnidades()
    {
        $query = "SELECT 
                    u.idUnidades AS id,
                    u.Nombre AS nombre,
                    u.Abreviatura AS abreviatura,
                    u.U_Base AS ubase
                    FROM unidades u";
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getListas()
    {
        $query = "SELECT 
                    l.idListas_Precio AS id,
                    l.Nombre AS nombre,
                    l.Porcentaje AS porcentaje,
                    l.Exclusiva AS exclusiva
                    FROM listas_precio l";
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getClientes($fecha)
    {
        $query = "SELECT 
                    cp.idcliente_profile AS id, 
                    IFNULL(cp.Codigo,\"N/A\") AS codigo, 
                    IF(cp.Nombre_Comercial=\"\", 'CONSUMIDOR FINAL', cp.Nombre_Comercial) AS nombre, 
                    c.Nit AS nit, 
                    IF(cp.Razon_Social=\"\", IF(cp.Nombre_Comercial=\"\", 'CONSUMIDOR FINAL', cp.Nombre_Comercial), cp.Razon_Social) AS razon,
                    cp.Email AS email, 
                    cp.Telefono_1 AS telefono, 
                    IF(cp.Direccion_Completa=\"\", 'CIUDAD', cp.Direccion_Completa) AS direccion, 
                    cp.DPI AS dpi, 
                    cp.Vendedor AS vendedor_id,
                    cp.Lista_Precio AS lista_id, 
                    cp.Tipo_Cliente AS tipo_id, 
                    cp.Ruta AS ruta_id, 
                    cp.Canal AS canal_id, 
                    cp.Ciudad AS ciudad_id, 
                    cp.Region AS region_id,
                    CONCAT(e.Nombre, \" \", e.Apellido) AS vendedor, 
                    l.Nombre AS lista, 
                    t.Nombre AS tipo, 
                    r.Nombre AS ruta, 
                    cana.Nombre AS canal,
                    ciu.Nombre AS ciudad, 
                    re.Nombre AS region, 
                    cc.Saldo AS saldo, 
                    cp.Latitud AS latitud, 
                    cp.Longitud AS longitud, 
                    concat(cp.Latitud, \",\", cp.Longitud),
                    cp.Activo AS activo
                    FROM cliente_profile cp
                    INNER JOIN clientes c ON cp.idcliente_profile=c.idClientes
                    INNER JOIN ciudades ciu ON ciu.idCiudad=cp.Ciudad
                    INNER JOIN empleados e ON e.idEmpleados=cp.Vendedor
                    INNER JOIN rutas r ON r.idRutas=cp.Ruta
                    INNER JOIN tipo_cliente t ON t.idTipo_Cliente=cp.Tipo_Cliente
                    INNER JOIN listas_precio l ON l.idListas_Precio=cp.Lista_Precio
                    INNER JOIN canales_ventas cana ON cana.idCanales_Ventas=cp.Canal
                    INNER JOIN regiones re ON re.idRegiones=cp.Region
                    INNER JOIN cuentas_corrientes cc  ON cc.idCuentas_Corrientes=cp.Cuenta_Corriente
                    WHERE 
                    cp.Fecha_Actualizacion > '" . $fecha . "'
                    GROUP BY cp.idcliente_profile";
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getProductos($now)
    {
        $query = "SELECT 
                    p.idProductos AS id,
                    p.Codigo AS codigo,
                    p.Nombre AS nombre,
                    p.Codigo_Barras AS barras,
                    p.Codigo_Proveedor AS codigo_proveedor,
                    p.Marca AS marca_id,
                    p.Proveedor AS proveedor_id,
                    p.Costo_Bruto AS costo,
                    m.Nombre AS marca,
                    prov.Nombre_Comercial AS proveedor,
                    u.Nombre AS unidad,
                    pu.Existencia AS existencia,
                    '" . $now . "' AS last_synch,
                    p.Activo AS activo,
                    p.Oferta AS oferta
                    FROM productos p
                    INNER JOIN marcas m ON m.idMarcas=p.Marca
                    INNER JOIN proveedores prov ON prov.idProveedores=p.Proveedor
                    INNER JOIN unidades u ON u.idUnidades=p.Unidad_Base
                    INNER JOIN productos_ubicaciones pu ON pu.Productos=p.idProductos AND pu.Defecto=1
                    GROUP BY p.idProductos";
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getPrecios($now)
    {
        $query = "SELECT
                    pp.Producto AS producto_id,
                    pp.Listas_Precio AS lista_id,
                    pp.Unidad AS unidad_id,
                    pp.Porcentaje AS porcentaje,
                    pp.Precio AS precio,
                    pp.Cantidad_Minima AS minima, 
                    p.Nombre AS producto,
                    l.Nombre AS lista,
                    u.Nombre AS unidad,
                    '" . $now . "' AS last_synch
                    FROM precios_productos pp
                    INNER JOIN productos p ON p.idProductos=pp.Producto
                    INNER JOIN listas_precio l ON l.idListas_Precio=pp.Listas_Precio
                    INNER JOIN unidades u ON u.idUnidades=pp.Unidad";
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getVentas($fecha, $start, $take)
    {
        $query = "SELECT 
                    f.idFacturas AS id,
                    f.Fecha AS fecha,
                    f.Serie AS serie,
                    f.Numero AS numero,
                    p.Observaciones AS detalle,
                    f.Total AS total,
                    p.Estado AS estado,
                    p.Cliente AS cliente_id,
                    p.Ruta AS ruta_id,
                    p.Bodega AS bodega_id,
                    p.Usuario AS usuario_id,
                    e.idEmpleados AS vendedor_id,
                    concat(e.nombre, ' ', e.apellido) as vendedor,
                    b.Nombre AS bodega,
                    IF(r.Nombre='', 'DEFAULT', r.Nombre) AS ruta,
                    f.Cliente AS cliente,
                    u.Nombre AS usuario
                    FROM facturas f
                    INNER JOIN pedidos p ON f.Pedido=p.idPedidos
                    INNER JOIN bodegas b ON p.Bodega=b.idBodegas
                    INNER JOIN rutas r ON p.Ruta=r.idRutas
                    INNER JOIN empleados e ON p.Vendedor=e.idEmpleados
                    INNER JOIN usuarios u ON u.idUsuarios=p.Usuario
                    INNER JOIN ventas v ON p.idPedidos=v.idVentas
                    WHERE p.Estado='Finalizado'
                    AND v.Fecha_Hora > '" . $fecha . "'
                    GROUP BY f.idFacturas ORDER BY f.idFacturas LIMIT ".$start.",".$take;
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getDetalleVentas($fecha, $start, $take)
    {
        $query = "SELECT 
                    l.idLinea_Detalle_Pedidos as id,
                    l.Cantidad as cantidad,
                    l.Producto as producto_id,
                    l.Unidad as unidad_id,
                    p.idPedidos as pedido_id,
                    f.idFacturas as factura_id,
                    l.Lista_Precio as lista_id,
                    ps.Nombre as producto,
                    l.Detalle as detalle,
                    u.Nombre as unidad,
                    lis.Nombre as lista,
                    l.Precio as precio,
                    l.Descuento as descuento,
                    0.00 as impuestos,
                    l.Bonificacion as bonificacion,
                    l.Secuencia_Repeticion as secuencia,
                    l.No_Linea as linea
                    FROM facturas f
                    INNER JOIN pedidos p ON f.Pedido=p.idPedidos
                    INNER JOIN linea_detalle_pedidos l ON l.Pedido=.p.idPedidos
                    INNER JOIN productos ps ON ps.idProductos=l.Producto
                    INNER JOIN unidades u ON u.idUnidades=l.Unidad
                    INNER JOIN listas_precio lis ON lis.idListas_Precio=l.Lista_Precio
                    INNER JOIN ventas v ON p.idPedidos=v.idVentas
                    WHERE p.Estado='Finalizado'
                    AND v.Fecha_Hora > '" . $fecha . "'
                    GROUP BY l.idLinea_Detalle_Pedidos LIMIT ".$start.",".$take;
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getCuentasPorCobrar($fecha, $start, $take)
    {
        $query = "SELECT 
                    cc.idCuentas_Por_Cobrar as id,
                    cc.Fecha_Vencimiento as fecha_vencimiento,
                    concat(f.Serie,'-',f.Numero) as facturas,
                    f.Fecha as fecha,
                    cc.Total as total,
                    cc.Pagado as pagado,
                    cc.Estado as estado,
                    '' as detalle,
                    c.idClientes as cliente_id,
                    r.idRutas as ruta_id,
                    b.idBodegas as bodega_id,
                    u.idUsuarios as usuario_id,
                    e.idEmpleados as empleado_id,
                    b.Nombre as bodega,
                    r.Nombre as ruta,
                    c.Nombre as cliente,
                    u.Nombre as usuario,
                    concat(e.Nombre, ' ', e.Apellido) as vendedor
                    FROM cuentas_por_cobrar cc
                    INNER JOIN movimientos_cuentas_por_cobrar m ON m.cuenta=cc.idCuentas_Por_Cobrar
                    INNER JOIN pedidos p ON cc.Documento=p.idPedidos
                    INNER JOIN facturas f ON f.Pedido=p.idPedidos
                    INNER JOIN bodegas b ON p.Bodega=b.idBodegas
                    INNER JOIN rutas r ON p.Ruta=r.idRutas
                    INNER JOIN empleados e ON p.Vendedor=e.idEmpleados
                    INNER JOIN usuarios u ON u.idUsuarios=p.Usuario
                    INNER JOIN clientes c ON p.Cliente=c.idClientes
                    WHERE m.fecha > '" . $fecha . "'
                    GROUP BY cc.idCuentas_Por_Cobrar LIMIT ".$start.",".$take;
        $result = DB::connection($this->database)->select($query);
        return $result;
    }

    private function getVentasCount($fecha) {
        $query = "SELECT count(*) as rows from
                    (SELECT 
                    f.idFacturas
                    FROM facturas f
                    INNER JOIN pedidos p ON f.Pedido=p.idPedidos
                    INNER JOIN bodegas b ON p.Bodega=b.idBodegas
                    INNER JOIN rutas r ON p.Ruta=r.idRutas
                    INNER JOIN usuarios u ON u.idUsuarios=p.Usuario
                    INNER JOIN ventas v ON p.idPedidos=v.idVentas
                    WHERE p.Estado='Finalizado'
                    AND v.Fecha_Hora > '" . $fecha . "'
                    GROUP BY f.idFacturas) a";
        return DB::connection($this->database)->select($query)[0]->rows;
    }

    private function getDetalleVentasCount($fecha) {
        $query = "SELECT count(*) as rows from
                    (SELECT l.idLinea_Detalle_Pedidos
                    FROM facturas f
                    INNER JOIN pedidos p ON f.Pedido=p.idPedidos
                    INNER JOIN linea_detalle_pedidos l ON l.Pedido=.p.idPedidos
                    INNER JOIN ventas v ON p.idPedidos=v.idVentas
                    WHERE p.Estado='Finalizado'
                    AND v.Fecha_Hora > '" . $fecha . "'
                    GROUP BY l.idLinea_Detalle_Pedidos) a";
        return DB::connection($this->database)->select($query)[0]->rows;
    }

    private function getCuentasPorCobrarCount($fecha) {
        $query = "SELECT count(*) as rows from
                    (SELECT cc.idCuentas_Por_Cobrar
                    FROM cuentas_por_cobrar cc
                    INNER JOIN movimientos_cuentas_por_cobrar m ON m.cuenta=cc.idCuentas_Por_Cobrar
                    WHERE m.fecha > '" . $fecha . "'
                    GROUP BY cc.idCuentas_Por_Cobrar) a";
        return DB::connection($this->database)->select($query)[0]->rows;
    }

    private function sendUnidades($token)
    {
        Log::info('-------------SINCRONIZANDO UNIDADES---------------');
        Log::info('Preparando envío de unidades');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $database = $this->database;

        $unidades = $this->getUnidades();
        Log::info('Registros encontrados en base de datos local: ' . count($unidades));
        Log::info('Conectando con el servidor...');
        $response = $http->post(self::API_URL . '/api/synch/unidades', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'database' => $database,
                'unidades' => $unidades
            ]
        ]);
        return $response->getBody()->getContents();
    }

    private function sendListas($token)
    {
        Log::info('-------------SINCRONIZANDO LISTAS DE PRECIO---------------');
        Log::info('Preparando envío de listas de precios');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $database = $this->database;

        $listas = $this->getListas();
        Log::info('Registros encontrados en base de datos local: ' . count($listas));
        Log::info('Conectando con el servidor...');
        $response = $http->post(self::API_URL . '/api/synch/listas', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'database' => $database,
                'listas' => $listas
            ]
        ]);
        return $response->getBody()->getContents();
    }

    private function sendClientes($token, $fecha)
    {
        Log::info('-------------SINCRONIZANDO CLIENTES---------------');
        Log::info('Preparando envío de clientes');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $database = $this->database;

        $clientes = $this->getClientes($fecha);
        Log::info('Registros encontrados en base de datos local: ' . count($clientes));
        Log::info('Conectando con el servidor...');
        $response = $http->post(self::API_URL . '/api/synch/clientes', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'database' => $database,
                'clientes' => $clientes
            ]
        ]);
        return $response->getBody()->getContents();;
    }

    private function sendProductos($token, $now)
    {
        Log::info('-------------SINCRONIZANDO PRODUCTOS---------------');
        Log::info('Preparando envío de productos');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $database = $this->database;

        $productos = $this->getProductos($now);
        Log::info('Registros encontrados en base de datos local: ' . count($productos));
        Log::info('Conectando con el servidor...');
        $response = $http->post(self::API_URL . '/api/synch/productos', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'database' => $database,
                'productos' => $productos
            ]
        ]);
        return $response->getBody()->getContents();;
    }

    private function sendPrecios($token, $now)
    {
        Log::info('-------------SINCRONIZANDO PRECIOS---------------');
        Log::info('Preparando envío de precios');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $database = $this->database;

        $precios = $this->getPrecios($now);
        Log::info('Registros encontrados en base de datos local: ' . count($precios));
        Log::info('Conectando con el servidor...');
        $response = $http->post(self::API_URL . '/api/synch/precios', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'database' => $database,
                'precios' => $precios
            ]
        ]);
        return $response->getBody()->getContents();
    }

    private function sendVentas($token, $fecha)
    {
        Log::info('-------------SINCRONIZANDO VENTAS-FACTURAS---------------');
        Log::info('Preparando envío de ventas');
        $database = $this->database;
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $max = 1000;
        $total = $this->getVentasCount($fecha);
        $pages = ceil($total / $max);

        Log::info('Registros encontrados en base de datos local: ' . $total.' Paginando 1000 resultados, No. páginas: '.$pages);
        Log::info('Conectando con el servidor...');

        for ($i = 1; $i < ($pages + 1); $i++) {
            $offset = (($i - 1)  * $max);
            $start = ($offset == 0 ? 0 : ($offset + 1));
            $ventas = $this->getVentas($fecha, $start, $max);
            $response = $http->post(self::API_URL . '/api/synch/ventas', [
                'headers' => ['Authorization' => 'Bearer ' . $token],
                RequestOptions::JSON => [
                    'database' => $database,
                    'ventas' => $ventas
                ]
            ]);
        }
        if(isset($response))
            return $response->getBody()->getContents();
        else
            return '{success:success}';
    }

    private function sendDetalleVentas($token, $fecha)
    {
        Log::info('-------------SINCRONIZANDO DETALLE-VENTAS-FACTURAS---------------');
        Log::info('Preparando envío de detalle ventas');
        $database = $this->database;
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $max = 1000;
        $total = $this->getDetalleVentasCount($fecha);
        $pages = ceil($total / $max);

        Log::info('Registros encontrados en base de datos local: ' . $total.' Paginando 1000 resultados, No. páginas: '.$pages);
        Log::info('Conectando con el servidor...');

        for ($i = 1; $i < ($pages + 1); $i++) {
            $offset = (($i - 1)  * $max);
            $start = ($offset == 0 ? 0 : ($offset + 1));
            $ventas = $this->getDetalleVentas($fecha, $start, $max);
            $response = $http->post(self::API_URL . '/api/synch/detalle_ventas', [
                'headers' => ['Authorization' => 'Bearer ' . $token],
                RequestOptions::JSON => [
                    'database' => $database,
                    'detalles' => $ventas
                ]
            ]);
        }
        if(isset($response))
            return $response->getBody()->getContents();
        else
            return '{success:success}';
    }

    private function sendCuentasPorCobrar($token, $fecha)
    {
        Log::info('-------------SINCRONIZANDO CUENTAS POR COBRAR---------------');
        Log::info('Preparando envío de cuentas por cobrar');
        $database = $this->database;
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $max = 1000;
        $total = $this->getCuentasPorCobrarCount($fecha);
        $pages = ceil($total / $max);

        Log::info('Registros encontrados en base de datos local: ' . $total.' Paginando 1000 resultados, No. páginas: '.$pages);
        Log::info('Conectando con el servidor...');

        for ($i = 1; $i < ($pages + 1); $i++) {
            $offset = (($i - 1)  * $max);
            $start = ($offset == 0 ? 0 : ($offset + 1));
            $cc = $this->getCuentasPorCobrar($fecha, $start, $max);
            $response = $http->post(self::API_URL . '/api/synch/cuentas_por_cobrar', [
                'headers' => ['Authorization' => 'Bearer ' . $token],
                RequestOptions::JSON => [
                    'database' => $database,
                    'cuentas' => $cc
                ]
            ]);
        }
        if(isset($response))
            return $response->getBody()->getContents();
        else
            return '{success:success}';
    }

    private function requestToken($request)
    {
        $http = new \GuzzleHttp\Client(['verify' => false]);

        $response = $http->post(self::API_URL . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'gEp8ddi7m62teJKwu0rjHNNkAGdjmbKzTVMWySNP',
                'username' => $request->get('user'),
                'password' => $request->get('password'),
                'scope' => '*',
            ],
        ]);
        return json_decode((string)$response->getBody(), true);
    }

    private function synchPedidosStatus($guardados, $token, $now) {
        Log::info('-------------SINCRONIZACION DE PEDIDOS---------------');
        Log::info('Preparando sincronizacion de pedidos');
        $database = $this->database;
        Log::info('Conectando con el servidor...');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        Log::info('Sincronizando pedidos.');
        $response = "";
        foreach ($guardados as $pedido) {
            Log::info('Conectando con el servidor...');
            $response = $http->put(self::API_URL . '/api/synch/pedidos', [
                'headers' => ['Authorization' => 'Bearer ' . $token],
                RequestOptions::JSON => [
                    'database' => $database,
                    'id_global' => $pedido['id_global'],
                    'id' => $pedido['id'],
                    'now' => $now
                ]
            ]);
            Log::info('Server Response: ' . $response->getBody());
            Log::info('Pedido sincronizado id_global: ' . $pedido['id_global'] . ' id_remoto: ' . $pedido['id']);
        }
        return $response;
    }

    private function downloadPedidos($token, $now)
    {
        Log::info('-------------DESCARGA DE PEDIDOS---------------');
        Log::info('Preparando descarga de pedidos');
        $database = $this->database;
        Log::info('Iniciando transacción');
        try {
            Log::info('Conectando con el servidor...');
            $http = new \GuzzleHttp\Client(['verify' => false]);
            $pediosResponse = $http->get(self::API_URL . '/api/synch/pedidos', [
                'headers' => ['Authorization' => 'Bearer ' . $token],
                RequestOptions::JSON => [
                    'database' => $database
                ]
            ]);
            $pedidos = json_decode((string)$pediosResponse->getBody(), true);

            Log::info('Pedidos obtenidos, cantidad de registros: ' . count($pedidos));
            $guardados = array();
            foreach ($pedidos as $pedido) {
                try {
                    DB::connection($this->database)->beginTransaction();
                    $id = $this->savePedido($pedido);
                    Log::info('Guardando pedido No.: ' . $id);
                    $detalles = $pedido['lineas'];
                    Log::info('Guardando detalles del pedido');
                    foreach ($detalles as $detalle) {
                        $this->saveDetalle($detalle, $id);
                    }
                    array_push($guardados, ['id' => $pedido['id'], 'id_global' => $id]);
                    DB::connection($this->database)->commit();
                    $response = $this->synchPedidosStatus([['id' => $pedido['id'], 'id_global' => $id]], $token, $now);
                    Log::info('Server Response: ' . $response->getBody()->getContents());
                } catch (\Exception $e) {
                    Log::info('Error en descarga de pedidos, haciendo rollback()');
                    DB::connection($this->database)->rollback();
                    throw $e;
                }
            }
            Log::info('Persistiendo todos los datos. commit()');
            Log::info('Pedidos guardados satisfactoriamente.');

            return $guardados;
        } catch (\Exception $e) {
            Log::info('Error en descarga de pedidos, haciendo rollback()');
            throw $e;
        }
    }

    private function savePedido($pedido)
    {
        $query = "INSERT INTO pedidos (Fecha, Cliente, Vendedor, Total_Lineas,"
            . " Hora_Inicio, Sub_Total,"
            . " Descuento, Total, Observaciones, Estado,"
            . " Bodega, Usuario, Terminal, Ruta)"
            . " VALUES (:fecha,:cliente_id,:vendedor_id,:total_lineas,:created_at,"
            . ":subtotal,:descuento,:total,:observaciones,'En Proceso',:bodega_id,:usuario_id,:terminal,:ruta_id)";
        $query = str_replace(':fecha', "'" . $pedido['fecha'] . "'", $query);
        $query = str_replace(':cliente_id', $pedido['cliente_id'], $query);
        $query = str_replace(':vendedor_id', $pedido['vendedor_id'], $query);
        $query = str_replace(':total_lineas', $pedido['total_lineas'], $query);
        $query = str_replace(':created_at', "'" . $pedido['created_at'] . "'", $query);
        $query = str_replace(':subtotal', $pedido['subtotal'], $query);
        $query = str_replace(':descuento', $pedido['descuento'], $query);
        $query = str_replace(':total', $pedido['total'], $query);
        $query = str_replace(':observaciones', "'" . $pedido['observaciones'] . "'", $query);
        $query = str_replace(':bodega_id', $pedido['bodega_id'], $query);
        $query = str_replace(':usuario_id', $pedido['usuario_id'], $query);
        $query = str_replace(':ruta_id', $pedido['ruta_id'], $query);
        $query = str_replace(':terminal', env('TERMINAL_LOCAL'), $query);
        DB::connection($this->database)->insert($query);
        $id = DB::connection($this->database)->getPdo()->lastInsertId();
        return $id;
    }

    private function saveDetalle($detalle, $id_pedido)
    {
        $query = "INSERT INTO linea_detalle_pedidos (Pedido, Producto, Cantidad, Precio, Unidad, Descuento, Secuencia_Repeticion, No_Linea, Detalle, Lista_Precio, Bonificacion)"
            . " VALUES (:id_pedido,:producto_id,:cantidad,:precio,:unidad_id,:descuento,:secuencia,:linea,:detalle,:lista_id, :bonificacion)";
        $detalle['id_pedido'] = $id_pedido;
        $query = str_replace(':id_pedido', $detalle['id_pedido'], $query);
        $query = str_replace(':producto_id', $detalle['producto_id'], $query);
        $query = str_replace(':cantidad', $detalle['cantidad'], $query);
        $query = str_replace(':precio', $detalle['precio'], $query);
        $query = str_replace(':unidad_id', $detalle['unidad_id'], $query);
        $query = str_replace(':descuento', $detalle['descuento'], $query);
        $query = str_replace(':secuencia', $detalle['secuencia'], $query);
        $query = str_replace(':linea', $detalle['linea'], $query);
        $query = str_replace(':detalle', "'" . $detalle['detalle'] . "'", $query);
        $query = str_replace(':lista_id', $detalle['lista_id'], $query);
        $query = str_replace(':bonificacion', $detalle['bonificacion'], $query);
        return DB::connection($this->database)->insert($query);
    }

    private function getLastSynch()
    {
        $http = new \GuzzleHttp\Client(['verify' => false]);

        $response = $http->get(self::API_URL . '/api/last_synch/' . $this->database);
        return $response->getBody();
    }

    private function getDatabase($token)
    {
        $http = new \GuzzleHttp\Client(['verify' => false]);
        $response = $http->get(self::API_URL . '/api/synch/database', [
            'headers' => ['Authorization' => 'Bearer ' . $token]
        ]);
        Log::info('Obtenido base de datos: '.$response->getBody());
        return $response->getBody();
    }

    public function index()
    {
        $last = Carbon::parse($this->getLastSynch());
        return view('web.index', ['last_synch' => $last->format('d-m-Y H:i:s')]);
    }

    public function logs()
    {
        $files = Storage::disk('logs')->files();
        $files = array_diff($files, ['.gitignore', 'laravel.log']);
        return view('web.log', ['logs' => $files]);
    }

    public function showLog($log)
    {
        return response()->download(storage_path('logs/' . $log . '.log'));
    }

    private function setupLog($now)
    {
        $log_file = '' . $now->year . $now->month . $now->day . $now->hour . $now->minute . $now->second . '_MAS_WEB_' . $this->database;
        Log::useFiles(storage_path() . '/logs/' . $log_file . '.log');
        return $log_file;
    }

    public function getLastLog(Request $request)
    {
        $file = File::get(storage_path() . '/logs/' . $request->get('log') . '.log');
        return $file;
    }

    private function updateLastSynchDate($token, $now)
    {
        $database = $this->database;
        Log::info('Sincronizando fecha de actualizacion');
        $http = new \GuzzleHttp\Client(['verify' => false]);
        Log::info('Conectando con el servidor...');
        $response = $http->put(self::API_URL . '/api/last_synch', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            RequestOptions::JSON => [
                'now' => $now,
                'database' => $database
            ]
        ]);
        return $response->getBody();
    }

    public function doSynch(Request $request)
    {
        //_____________VARIABLES DE SINCRONIZACION________________
        $now = Carbon::now();
        $log_file = $this->setupLog($now);

        //_____________INICIALIZACION________________
        Log::info('Sincronizacion iniciada.');
        Log::info('Validando ususario: ' . $request->get('user'));

        //_____________AUTENTICACIÓN________________
        try {
            $token = $this->requestToken($request);
            Log::info('Usuario validado. Se obtuvo un api_token');
            $db = $this->getDatabase($token['access_token']);
            if(config()->has('database.connections.'.$db)){
                $this->database = $db."";
            }
            $last = $this->getLastSynch();
            Log::info('Ultima sincronizacion: ' . $last);
            Log::info('Configuracion de base de datos: '.$this->database);
        } catch (\Exception $e) {
            Log::error('Error de validacion de usuario');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Usuario no autorizado', 'log' => $log_file], 401);
        }

        //_____________ENVÍO DE UNIDADES________________
        try {
            $response = $this->sendUnidades($token['access_token']);
            Log::info('Server Response: ' . $response);
            Log::info('Unidades enviadas satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:unidades).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________ENVÍO DE LISTAS DE PRECIOS________________
        try {
            $response = $this->sendListas($token['access_token']);
            Log::info('Server Response: ' . $response);
            Log::info('Listas de precio enviadas satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:listas_precio).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________ENVÍO DE CLIENTES________________
        try {
            $response = $this->sendClientes($token['access_token'], $last);
            Log::info('Server Response: ' . $response);
            Log::info('Clientes enviados satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:cliente_profile).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________ENVÍO DE PRODUCTOS________________
        try {
            $response = $this->sendProductos($token['access_token'], $now->format('Y-m-d H:i:s'));
            Log::info('Server Response: ' . $response);
            Log::info('Productos enviados satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:productos).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________ENVÍO DE PRECIOS________________
        try {
            $response = $this->sendPrecios($token['access_token'], $now->format("Y-m-d"));
            Log::info('Server Response: ' . $response);
            Log::info('Precios enviados satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:precios_productos).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________ENVÍO DE VENTAS________________
        // try {
        //     $response = $this->sendVentas($token['access_token'], $last);
        //     Log::info('Server Response: ' . $response);
        //     Log::info('Ventas enviadas satisfactoriamente.');
        // } catch (\Exception $e) {
        //     Log::error('Error de envio de datos (Tabla:facturas,ventas).');
        //     Log::error($e->getMessage());
        //     Log::error($e->getTraceAsString());
        //     return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        // }

        //_____________ENVÍO DE DETALLE________________
        // try {
        //     $response = $this->sendDetalleVentas($token['access_token'], $last);
        //     Log::info('Server Response: ' . $response);
        //     Log::info('Detalle de ventas enviadas satisfactoriamente.');
        // } catch (\Exception $e) {
        //     Log::error('Error de envio de datos (Tabla:linea_detalle_pedidos).');
        //     Log::error($e->getMessage());
        //     Log::error($e->getTraceAsString());
        //     return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        // }

        //_____________ENVÍO DE CUENTAS POR COBRAR________________
        try {
            $response = $this->sendCuentasPorCobrar($token['access_token'], $last);
            Log::info('Server Response: ' . $response);
            Log::info('Cuentas Por Cobrar enviadas satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error de envio de datos (Tabla:cuentas_por_cobrar).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en envío de datos', 'log' => $log_file], 500);
        }

        //_____________DESCARGAR PEDIDOS________________
        try {
            $guardados = $this->downloadPedidos($token['access_token'], $now->format('Y-m-d H:i:s'));
            Log::info('Pedidos descargados satisfactoriamente.');
        } catch (\Exception $e) {
            Log::error('Error descargando pedidos (Tabla:pedidos,linea_detalle_pedidos).');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en descarga de datos', 'log' => $log_file], 500);
        }

        //_____________SINCRONIZACION DE PEDIDOS________________
//        try {
//            if(isset($guardados)) {
//                Log::info('Server Response: ' . $response);
//                Log::info('Pedidos sincronizados.');
//            }
//        } catch (\Exception $e) {
//            Log::error('Error sincronizando pedidos.');
//            Log::error($e->getMessage());
//            Log::error($e->getTraceAsString());
//            return response()->json(['message' => 'Error en sincronizacion de datos', 'log' => $log_file], 500);
//        }

        //______________ACTUALIZAR FECHA DE ÚLTIMA SINCRONIZACIÓN________________
        try {
            $response = $this->updateLastSynchDate($token['access_token'], $now->format('Y-m-d H:i:s'));
            Log::info('Server Response: ' . $response);
            Log::info('Fecha de ultima sincronización actualizada.');
        } catch (\Exception $e) {
            Log::error('Error actualizando fecha de sincronización.');
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return response()->json(['message' => 'Error en actualizar fecha de sincronización', 'log' => $log_file], 500);
        }

        Log::info('Fin de labores. Todo correcto.');
        return response()->json(['message' => 'Las peticiones se procesaron exitosamente', 'log' => $log_file, 'last' => $now->format('Y-m-d H:i:s')], 200);
    }

}
