<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servidor extends Model 
{   
    
    use SoftDeletes;
    
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'servidores';
    
    protected $dates = [
        'ultima_sincronizacion'
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'servidor_id',
        'sucursal_id',
        'sucursal_codigo',
        'sucursal_nombre',
        'ultima_sincronizacion'
        ];
}

