<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model 
{
    use SoftDeletes;
    
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inventarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'servidor_id',
            'producto_id',
            'codigo',
            'codigo_proveedor',
            'nombre',
            'proveedor_id',
            'proveedor_codigo',
            'proveedor_nombre',
            'sucursal_id',
            'sucursal_codigo',
            'sucursal_nombre',
            'bodega_id',
            'bodega_nombre',
            'unidad_id',
            'unidad_nombre',
            'unidad_base',
            'costo',
            'activo',
            'existencia',
            'ultima_compra'
        ];
}
