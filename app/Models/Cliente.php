<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model {

    use SoftDeletes;

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clientes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cliente_id', 'servidor_id',
        'codigo',
        'nombre_comercial',
        'razon_social',
        'nit',
        'direccion',
        'dpi',
        'telefono_1',
        'telefono_2',
        'email',
        'activo',
        'geopos',
        'vendedor_id',
        'vendedor_codigo',
        'vendedor_nombre',
        'ruta_id',
        'ruta_codigo',
        'ruta_nombre',
        'canal_id',
        'canal_codigo',
        'canal_nombre',
        'ciudad_id',
        'ciudad_codigo',
        'ciudad_nombre',
        'region_id',
        'region_codigo',
        'region_nombre',
        'municipio_id',
        'municipio_codigo',
        'municipio_nombre',
        'departamento_id',
        'departamento_codigo',
        'departamento_nombre',
        'lista_id',
        'lista_nombre',
        'tipo_id',
        'tipo_nombre'
    ];

}
