<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venta extends Model 
{
    use SoftDeletes;
    
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ventas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'servidor_id',
            'pedido_id',
            'factura',
            'fecha',
            'fecha_inicio',
            'fecha_fin',
            'vendedor_id',
            'vendedor_codigo',
            'vendedor_nombre',
            'ruta_id',
            'ruta_codigo',
            'ruta_nombre',
            'estado',
            'cliente_id',
            'cliente_codigo',
            'cliente_nombre',
            'cliente_nit',
            'cliente_direccion',
            'no_linea',
            'cantidad',
            'producto_id',
            'producto_codigo',
            'producto_nombre',
            'detalle',
            'unidad_id',
            'unidad_nombre',
            'unidad_base',
            'precio',
            'costo',
            'descuento',
            'lista_id',
            'lista_nombre'
        ];
}
