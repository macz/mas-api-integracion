@section('styles')
    <link href="{{ asset("css/spinner.css") }}" rel="stylesheet">
@endsection
@extends('layout')
@section('content')
    <h3><i class="glyphicon glyphicon-book"></i> Consulta de Logs</h3>
    <hr>
    <div class="well">
        @if($logs)
        <ul>
            @foreach($logs as $log)
            <li><a href="{{ route('showLog', str_replace('.log', '', $log)) }}">{{ $log }}</a></li>
            @endforeach
        </ul>
        @else
        <p>No se encontraron archivos de log</p>
        @endif
    </div>
@endsection