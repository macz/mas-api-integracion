@section('styles')
    <link href="{{ asset("css/spinner.css") }}" rel="stylesheet">
@endsection
@extends('layout')
@section('content')
    <h3><i class="glyphicon glyphicon-upload"></i> Sincronización con la nube</h3>
    <hr>
    <p>Ultima sincronización: <span id="last">{{ $last_synch }}</span></p>
    <h4>Credenciales de sincronización</h4>
    <div class="login-form">
        <form id="send" action="{{ route('doSynch') }}">
        {!! Form::token() !!}
            <!-- if there are login errors, show them here -->
                <div id="info" class="alert" style="display: none">
                </div>
            <div class="form-group">
                <label>Usuario</label>
                <input id="user" name="user" type="text" class="form-control"/>
            </div>

            <div class="form-group">
                <label>Contraseña</label>
                <input id="password" name="password" type="password" class="form-control"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Enviar</button>
                <div id="spinner" class="loader" style="display: none"></div>
            </div>
        </form>
    </div>
    <div id="logs" style="display: none">
        <h4>Log output</h4>
        <textarea id="log" rows="10" cols="80" disabled>Listo.</textarea>
    </div>
    <a href="{{route('logs')}}"><i class="glyphicon glyphicon-book"></i>&nbsp;Consultar logs</a>
@endsection
@section('scripts')
    <script type="application/javascript">
        function showLog(log) {
            $("#logs").show();
            $.ajax({
                type: "GET",
                url: 'api/getLastLog?log='+log,
                success: function (data) {
                    $("#log").val(data);
                },
                error: function (error) {
                    $("#info").html(error.responseText);
                    $("#log").val(error.responseText);
                }
            });
        }

        $("#send").submit(function (e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            $("#info").hide();
            $("#logs").hide();
            var form = $(this);
            var dataJson = {};
            $.each(form.serializeArray(), function() { dataJson[this.name] = this.value; });
            var url = form.attr('action');
            $("#spinner").show();
            $("#send :input").prop("disabled", true);
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(dataJson), // serializes the form's elements.
                success: function (data) {
                    $("#spinner").hide();
                    $("#info").removeClass('alert-danger');
                    $("#info").addClass('alert-info');
                    $("#info").show();
                    $("#info").html(data.message);
                    $("#send :input").prop("disabled", false);
                    showLog(data.log);
                    if(data.last) {
                        $("#last").html(data.last);
                    }
                },
                error: function (error) {
                    $("#spinner").hide();
                    $("#info").removeClass('alert-info');
                    $("#info").addClass('alert-danger');
                    $("#info").show();
                    $("#send :input").prop("disabled", false);
                    showLog(error.responseJSON.log);
                    $("#info").html(error.responseJSON.message);
                    if(error.responseJSON.exception) {
                        console.log(error.responseJSON.exception);
                    }
                }
            });
        });
    </script>
@endsection