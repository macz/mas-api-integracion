<!DOCTYPE html>
<html lang={{ config('app.locale') }}>
      <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>MAS ERP Herramienta de integraci&oacute;n</title>
        <meta name="generator" content="mas" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="{{ asset("bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">
        <!--[if lt IE 9]>
                <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="{{ asset("css/styles.css") }}" rel="stylesheet">
        @yield('styles')
    </head>
    <body>
        <div class="wrapper">
            <div class="box">
                <div class="row row-offcanvas row-offcanvas-left">
                    <!-- sidebar -->
                    <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">

                        <ul class="nav">
                            <li><a href="#" data-toggle="offcanvas" class="visible-xs text-center"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
                        </ul>

                        <ul class="nav hidden-xs" id="lg-menu">
                            <li class="active"><a href="{{ url('/') }}"><i class="glyphicon glyphicon-list-alt"></i> Descargar aplicaci&oacute;n</a></li>
                            <li><a href="{{ route("integrador") }}"><i class="glyphicon glyphicon-cloud-upload"></i> Integraci&oacute;n</a></li>
                            <li><a href="{{ route("sincro") }}"><i class="glyphicon glyphicon-upload"></i> Sincronización</a></li>
                        </ul>

                        <!-- tiny only nav-->
                        <ul class="nav visible-xs" id="xs-menu">
                            <li><a href="{{ url('/') }}" class="text-center"><i class="glyphicon glyphicon-list-alt"></i></a></li>
                            <li><a href="{{ route("integrador") }}" class="text-center"><i class="glyphicon glyphicon-cloud-upload"></i></a></li>
                        </ul>

                    </div>
                    <!-- /sidebar -->

                    <!-- main right col -->
                    <div class="column col-sm-10 col-xs-11" id="main">

                        <!-- top nav -->
                        <div class="navbar navbar-blue navbar-static-top">
                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <nav class="nav navbar-form navbar-nav">
                                    <a href="{{ url('/') }}"><img src="{{ asset('img/icon.png') }}" width="32px" height="32px"></a>
                                </nav>
                            </div>
                            <nav class="collapse navbar-collapse" role="navigation">
                                <form class="navbar-form navbar-left">
<!--                                    <div class="input-group input-group-sm" style="max-width:360px;">
                                        <input type="text" class="form-control" placeholder="Buscar" name="srch-term" id="srch-term">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>-->
                                </form>
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                                    </li>
<!--                                    <li>
                                        <a href="#postModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Publicar</a>
                                    </li>-->
                                </ul>
                                <!--                    <ul class="nav navbar-nav navbar-right">
                                                      <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
                                                        <ul class="dropdown-menu">
                                                          <li><a href="">More</a></li>
                                                          <li><a href="">More</a></li>
                                                          <li><a href="">More</a></li>
                                                          <li><a href="">More</a></li>
                                                          <li><a href="">More</a></li>
                                                        </ul>
                                                      </li>
                                                    </ul>-->
                            </nav>
                        </div>
                        <!-- /top nav -->

                        <div class="padding">
                            <div class="full col-sm-9">
                                <!-- content -->
                                <div class="row"
                                     <!-- main col right -->
                                     <div class="col-sm-8">
                                        @yield('content')
                                    </div>

                                <!-- Right column content -->
                                <div class="col-sm-4">
                                    @yield('right_content')
                                </div>
                                </div>

                                <div class="row" id="footer">
                                    <div class="col-sm-6">

                                    </div>
                                    <div class="col-sm-6">
                                        <p>
                                            <a href="#" class="pull-right">©Copyright 2017</a>
                                        </p>
                                    </div>
                                </div>

                                <hr>
                            </div><!-- /col-9 -->
                        </div><!-- /padding -->
                    </div>
                    <!-- /main -->

                </div>
            </div>
        </div>

        @include('news/post_dialog')

        <!-- script references -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="{{ asset("bootstrap/js/bootstrap.min.js") }}"></script>
        <script src="{{ asset("js/scripts.js") }}"></script>
        @yield('scripts')
    </body>
</html>