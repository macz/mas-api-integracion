<div class="panel panel-default">
    <div class="panel-thumbnail">
        <a href="{{ asset('downloads/mas.apk') }}">
            <img src="{{ asset('img/icon.png') }}" class="img-responsive center-block">
        </a>
    </div>
    <div class="panel-body">
        <p class="lead text-center">Descargar la app</p>
    </div>
</div>