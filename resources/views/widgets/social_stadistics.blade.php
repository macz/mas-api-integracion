<div class="panel panel-default">
    <div class="panel-body">
        <p class="lead">Estad&iacute;sticas sociales</p>
        <p>{{ $followers or '0' }} Seguidores, {{ $posts or '0' }} Publicaciones</p>

        <p>
            <img src="https://facebookbrand.com/wp-content/themes/fb-branding/prj-fb-branding/assets/images/fb-art.png" width="28px" height="28px">
        </p>
    </div>
</div>