<div class="panel panel-default">
    <div class="panel-heading">
        <a href="#" class="pull-right">Ver m&aacute;s</a> 
        <h4>{{ $news->title }}</h4>
    </div>
    <div class="panel-body">
        <p><img src={{ asset($news->image) }}> <a href="#">{{ $news->user }}</a></p>
        <div class="clearfix"></div>
        <hr>
        {{ $news->shortText }}
    </div>
</div>