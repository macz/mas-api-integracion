@extends('layout')
@section('content')
    @if(isset($news))
        @include('news/news')
    @else
    <div class="panel panel-default">
        <div class="panel-body">
            <p>No se encontraron publicaciones</p>
        </div>
    </div>
    @endif
@endsection

@section('right_content')
@include('widgets/download_app')
@endsection