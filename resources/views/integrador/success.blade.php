@extends('layout')
@section('content')
<h3><i class="glyphicon glyphicon-signal"></i> Resultados de envío de información</h3>  
<hr>
<div class="row">
    <!-- center left-->	
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-check pull-right"></i>
                    <h4>Resumen de archivos enviados</h4>
                </div>
            </div>
            <div class="panel-body">
                @if(Session::has('alert'))
                <div id="alert" class="alert alert-success">
                    <i class="glyphicon glyphicon-exclamation-sign"></i>
                    <span>{{ Session::get('alert') }}</span>
                </div>
                @endif
                <div class="alert">
                    <h5>Empresa: {{ $empresa }}</h5>
                    <ul>
                    @foreach($archivos as $archivo)
                        <li>{{ $archivo }}</li>
                    @endforeach
                    </ul>
                </div>
                <div>
                    <a href="{{ $backup }}"><i class="fa fa-fw glyphicon glyphicon-download-alt"></i>&nbsp;Descargar archivos enviados</a>
                </div>
                <br>
                <br>
                <div>
                    <a href="{{ route('integrador') }}"> <i class="fa fa-fw glyphicon glyphicon-backward pull-left"></i>&nbsp;Regresar</a>
                </div>
            </div><!--/panel content-->
        </div><!--/panel-->
    </div><!--/col-->
</div><!--/row-->
@endsection