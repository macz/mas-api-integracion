@extends('layout')
@section('content')
<h3><i class="glyphicon glyphicon-wrench"></i> Configuración de interfaces de integración</h3>  
<hr>
<div class="row">
    <!-- center left-->	
    <div>
        {!! Form::open(['method' => 'POST', 'route' => 'guardar_configuracion', 'class' => 'form']) !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-edit pull-right"></i>
                    <h4>Configuración general empresa</h4>
                </div>
            </div>
            @if(Session::has('alert'))
            <div id="alert" class="alert alert-success">
                <i class="glyphicon glyphicon-exclamation-sign"></i>
                <span>{{ Session::get('alert') }}</span>
            </div>
            @endif
            <div class="panel-body">
                <div class="form-group row">
                    <label class="col-form-label" for="empresa">Seleccione una empresa</label>
                    {!! Form::select('empresa', $empresas, $empresa, ['id'=>'empresa', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group row">
                    <label for="patron">Patrón de archivo</label>
                    {!! Form::text('patron', $config['archivoPatron'], ['class' => 'form-control']) !!}
                    <small id="patronHelp" class="form-text text-muted">Parámetros: pais, mes, dia, anio, minutos, segundos, consulta. (Encerrados en '%')</small>
                </div>
                <div class="form-group row">
                    <label for="patronZip">Patrón de archivo comprimido</label>
                    {!! Form::text('patronZip', $config['ZIPPatron'], ['class' => 'form-control']) !!}
                    <small id="patronZipHelp" class="form-text text-muted">Parámetros: pais, mes, dia, anio, minutos, segundos, consulta. (Encerrados en '%')</small>
                </div>
                <div class="form-group row">
                    <label for="caracterSeparacion">Caracter separacion</label>
                    {!! Form::text('caracterSeparacion', $config['archivoCaracterSeparacion'], ['class' => 'form-control']) !!}
                    <small id="separacionHelp" class="form-text text-muted">Caracter que separa cada uno de los campos dentro de un archivo</small>
                </div>
                <div class="form-check row">
                    <label class="form-check-label" for="encabezado">Encabezado de archivo</label>
                    {!! Form::checkbox('encabezado', 'TRUE',($config['archivoEncabezado']=='TRUE' ? true : false), ['class' => 'form-check-input']) !!}
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="form-check-label" for="zip">Enviar todos los archivos comprimidos</label>
                    {!! Form::checkbox('zip', 'TRUE',($config['ZIP']=='TRUE' ? true : false), ['class' => 'form-check-input']) !!}
                </div>
                <hr>
                <h4>Parámetros del servidor FTP</h4>
                <div class="form-group row">
                    <label for="ftpServer">Servidor FTP</label>
                    {!! Form::text('ftpServer', $config['ftpServidor'], ['class' => 'form-control']) !!}
                    <small id="serverFTPHelp" class="form-text text-muted">URL o IP del servidor FTP</small>
                </div>
                <div class="form-group row">
                    <label for="ftpUser">Usuario</label>
                    {!! Form::text('ftpUser', $config['ftpUsuario'], ['class' => 'form-control']) !!}
                    <small id="ftpUserHelp" class="form-text text-muted">Usuario del servidor FTP</small>
                </div>
                <div class="form-group row">
                    <label for="ftpPass">Contraseña</label>
                    {!! Form::password('ftpPass', ['class' => 'form-control']) !!}
                    <small id="ftpPassHelp" class="form-text text-muted">Contraseña del servidor FTP</small>
                </div>
                <div class="form-group row">
                    <label for="ftpDir">Directorio FTP</label>
                    {!! Form::text('ftpDir', $config['ftpDirectorio'], ['class' => 'form-control']) !!}
                    <small id="ftpDirHelp" class="form-text text-muted">Directorio dentro del FTP donde se envían los archivos</small>
                </div>
                <hr>
                <h4>Parámetros del servidor de base de datos</h4>
                <div class="form-group row">
                    <label for="dbServer">Servidor de base de datos</label>
                    {!! Form::text('dbServer', $config['dbServidor'], ['class' => 'form-control']) !!}
                    <small id="dbServerHelp" class="form-text text-muted">URL o IP del servidor base de datos</small>
                </div>
                <div class="form-group row">
                    <label for="dbUser">Usuario</label>
                    {!! Form::text('dbUser', $config['dbUsuario'], ['class' => 'form-control']) !!}
                    <small id="dbUserHelp" class="form-text text-muted">Usuario del servidor base de datos</small>
                </div>
                <div class="form-group row">
                    <label for="dbPass">Contraseña</label>
                    {!! Form::password('dbPass', ['class' => 'form-control']) !!}
                    <small id="dbPassHelp" class="form-text text-muted">Contraseña del servidor base de datos</small>
                </div>
                <div class="form-group row">
                    <label for="dbName">Nombre de la base de datos</label>
                    {!! Form::text('dbName', $config['dbNombre'], ['class' => 'form-control']) !!}
                    <small id="dbNameHelp" class="form-text text-muted">Nombre del esquema de base de datos</small>
                </div>
            </div><!--/panel content-->
        </div><!--/panel-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-th-list pull-right"></i>
                    <h4>Configuración de archivos</h4>
                </div>
            </div>
            <div class="panel-body">
                <h4>Parámetros de configuración de archivos</h4>
                <hr>
                <div id='consultas'>
                    @foreach($config['consultas'] as $consulta)
                    <i class="glyphicon glyphicon-file pull-left"></i>
                    <h4>Nombre: <strong>{{$consulta->nombre}}</strong></h4>
                    <div class="form-group row">
                        <label for="query_{{$consulta->nombre}}">Consulta</label>
                        {!! Form::textarea('query_'.$consulta->nombre, $consulta->query , ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group row">
                        <label for="fechaultima_{{$consulta->nombre}}">Fecha de último envío</label>
                        <input class="form-control" type="datetime" value="{{ \Carbon\Carbon::parse($consulta->fechaUltimaExportacion)->format('d-m-Y H:i:s') }}" name="fechaultima_{{$consulta->nombre}}">
                    </div>
                    <hr>
                    @endforeach
                </div>
                <div class="control-group pull-left">
                    <div class="controls">
                        {!! Form::submit('Guardar', ['class'=>'btn btn-primary']) !!} 
                    </div>
                </div> 
                <a class='pull-right' href="{{ route('integrador') }}"> <i class="fa fa-fw glyphicon glyphicon-backward"></i>&nbsp;Regresar</a>
            </div><!--/panel content-->
        </div><!--/panel-->
        {!! Form::close() !!}
    </div><!--/col-->
</div><!--/row-->
@endsection
@section('scripts')
<script>
    $("#empresa").bind('change', function () {
        var url = '{{ route('configurar_integrador') }}' + '/' + $(this).val();
        if (url) {
            window.location = url;
        }
        return false;
    });
    $('#alert').delay(3000).slideUp(300);
</script>
@endsection