@extends('layout')
@section('content')
<h3><i class="glyphicon glyphicon-dashboard"></i> Ejecutar Integración</h3>  
<hr>
<div class="row">
    <!-- center left-->	
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="glyphicon glyphicon-send pull-right"></i>
                    <h4>Envío de archivos de integración</h4>
                </div>
            </div>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Se encontraron los siguientes errores:<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {!! Form::open(['method' => 'POST', 'route' => 'send', 'class' => 'form form-vertical']) !!}
                    <div class="control-group">
                        <label>Seleccione una empresa</label>
                        <div class="controls">
                            <select class="form-control" name="empresa">
                                @foreach($empresas as $empresa)
                                <option value="{{$empresa}}">{{ $empresa }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>    
                    <div class="control-group">
                        <label></label>
                        <div class="controls">
                            {!! Form::submit('Enviar', ['class'=>'btn btn-primary']) !!} 
                        </div>
                    </div> 
                <div class="pull-right">
                    <a href="{{ route('configurar_integrador') }}"> <i class="fa fa-fw glyphicon glyphicon-wrench"></i>&nbsp;Configurar parámetros de envío</a>
                </div>
                {!! Form::close() !!}
            </div><!--/panel content-->
        </div><!--/panel-->
    </div><!--/col-->
</div><!--/row-->
@endsection
@section('scripts')
<script>
$('#alert').delay(3000).slideUp(300);
</script>
@endsection