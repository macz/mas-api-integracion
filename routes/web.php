<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('integrador', [
    'uses' => 'IntegradorController@index',
    'as' => 'integrador'
]);

Route::post('integrador/enviar', [
    'uses' => 'IntegradorController@enviar',
    'as' => 'send'
]);

Route::get('integrador/configurar/{empresa?}', [
    'uses' => 'IntegradorController@configurar',
    'as' => 'configurar_integrador'
]);

Route::get('sincro/', [
    'uses' => 'MasWebController@index',
    'as' => 'sincro'
]);

Route::get('showLog/{log}', [
    'uses' => 'MasWebController@showLog',
    'as' => 'showLog'
]);

Route::get('logs', [
    'uses' => 'MasWebController@logs',
    'as' => 'logs'
]);

Route::post('integrador/configurar/guardar', [
    'uses' => 'IntegradorController@guardarConfiguracion',
    'as' => 'guardar_configuracion'
]);