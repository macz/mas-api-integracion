<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Resources of mobil app
|--------------------------------------------------------------------------
*/

Route::post('agendas', 'ResourcesController@getAgendas');
Route::post('bodegas', 'ResourcesController@getBodegas');
Route::post('categorias', 'ResourcesController@getCategorias');
Route::post('clientes', 'ResourcesController@getClientes');
Route::post('detalle-ofertas', 'ResourcesController@getDetalleOfertas');
Route::post('listas-precio', 'ResourcesController@getListasPrecio');
Route::post('ofertas', 'ResourcesController@getOfertas');
Route::post('precios', 'ResourcesController@getPrecios');
Route::post('productos', 'ResourcesController@getProductos');
Route::post('tipo-clientes', 'ResourcesController@getTipoClientes');
Route::post('unidades', 'ResourcesController@getUnidades');
Route::post('usuarios', 'ResourcesController@getUsuarios');
Route::post('pedidos', 'ResourcesController@persistPedidos');

Route::get('apk-version', 'UpdateController@getLatestVersion');

/*
|--------------------------------------------------------------------------
| API Sincronizacion data servers Version 1.0
|--------------------------------------------------------------------------
*/
Route::get('v1/sincro/get_fecha_sincro/{server_id}', 'SincroController@getFechaUltimaExportacionServer');
Route::post('v1/sincro/guardar_data', 'SincroController@guardarData');
Route::get('v1/sincro/enviar/{database?}', 'SincroController@enviarData');
Route::post('v1/integrador/enviar', 'IntegradorController@enviar');


Route::post('sincro/do', [
    'uses' => 'MasWebController@doSynch',
    'as' => 'doSynch'
]);

Route::get('getLastLog', [
    'uses' => 'MasWebController@getLastLog',
    'as' => 'getLastLog'
]);